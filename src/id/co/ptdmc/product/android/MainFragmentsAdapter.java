package id.co.ptdmc.product.android;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Fragment adapter for {@link MainActivity}.
 *
 * @author Rochmat Santoso
 * */
public final class MainFragmentsAdapter extends FragmentPagerAdapter {

    /**
     * Total number of fragment.
     * */
    private static final int TOTAL_FRAGMENT = 3;

    /**
     * List of fragment objects.
     * */
    private List<Fragment> listFragments;

    /**
     * @param fm the fragment manager
     * */
    public MainFragmentsAdapter(final FragmentManager fm) {
        super(fm);

        listFragments = new ArrayList<>();
        listFragments.add(new MainSearchFragment());
        listFragments.add(new MainPhotoFragment());
        listFragments.add(new MainStatusFragment());
    }

    @Override
    public Fragment getItem(final int position) {
        Fragment result = listFragments.get(position);
        return result;
    }

    @Override
    public int getCount() {
        return TOTAL_FRAGMENT;
    }

}
