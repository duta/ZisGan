package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.models.Comment;
import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.BitmapCacheUtil;
import id.co.ptdmc.product.android.utils.CommonUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * An adapter class for displaying a list of comment.
 *
 * @author Rochmat Santoso
 * */
public final class DetailContentAdapter extends ArrayAdapter<Comment> {

    /**
     * Application context.
     * */
    private Context context;

    /**
     * List of comment.
     * */
    private List<Comment> data;

    /**
     * @param ctx application context
     * @param objects the data to display
     * */
    public DetailContentAdapter(
            final Context ctx, final List<Comment> objects) {
        super(ctx, R.layout.fragment_detail_content, objects);

        this.context = ctx;
        this.data = objects;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(
            final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_detail_content, null);

            ViewHolder holder = new ViewHolder();
            holder.imgViewProfile = (ImageView) convertView.findViewById(R.id.imgViewProfile);
            holder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            holder.txtComment = (TextView) convertView.findViewById(R.id.txtComment);
            holder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);

            convertView.setTag(holder);
        }

        final Comment obj = data.get(position);
        String name = obj.getCommentUserName();
        String comment = obj.getCommentText();
        String date = CommonUtil.formatDateInFriendlyFormat(obj.getCommentDate());

        final ViewHolder holder = (ViewHolder) convertView.getTag();
        if (holder.task != null) {
            holder.task.cancel(true);
        }
        holder.txtName.setText(name);
        holder.txtComment.setText(comment);
        holder.txtDate.setText(date);

        AsyncTask<Post, Integer, Bitmap> task = new AsyncTask<Post, Integer, Bitmap>() {
            @Override
            protected Bitmap doInBackground(final Post... params) {
                try {
                    BitmapCacheUtil cache = BitmapCacheUtil.getInstance();
                    Bitmap bmp = cache.getBitmapFromCache(obj.getCommentUserProfile());
                    if (bmp == null) {
                        URL url = new URL("https://graph.facebook.com/v2.2/" + obj.getCommentUserProfile() + "/picture");
                        InputStream stream = url.openConnection().getInputStream();
                        Bitmap bitmap = BitmapFactory.decodeStream(stream);
                        cache.addBitmapToCache(obj.getCommentUserProfile(), bitmap);
                        return bitmap;
                    } else {
                        return bmp;
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Bitmap bitmap) {
                holder.imgViewProfile.setImageBitmap(bitmap);
            }
        };
        holder.task = task;

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }

        return convertView;
    }

    /**
     * A helper class that contains widgets to display thumbnail image.
     * */
    static class ViewHolder {
        /**
         * The profile image.
         * */
        private ImageView imgViewProfile;

        /**
         * The profile name.
         * */
        private TextView txtName;

        /**
         * The comment.
         * */
        private TextView txtComment;

        /**
         * The comment date.
         * */
        private TextView txtDate;

        /**
         * The task to load profile image.
         * */
        private AsyncTask<Post, Integer, Bitmap> task;
    }

}
