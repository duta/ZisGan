package id.co.ptdmc.product.android.daos;

import id.co.ptdmc.product.android.models.IBean;

import java.util.List;
import java.util.Map;

/**
 * Base DAO for all DAO classes.
 *
 * @param <X> the bean data model
 * @author Rochmat Santoso
 * */
public interface IDao<X extends IBean> {

    /**
     * Select data by filter.
     *
     * @param filter key value map that contains filter
     * @return a list of bean
     * */
    List<X> selectByFilter(Map<String, Object> filter);

    /**
     * Insert bean data model to database.
     *
     * @param x the bean to insert
     * */
    void insert(X x);

    /**
     * Update bean data model.
     *
     * @param x the bean to update
     * */
    void update(X x);

    /**
     * Delete bean data model.
     *
     * @param x the bean to delete
     * */
    void delete(X x);

}
