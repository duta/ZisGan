package id.co.ptdmc.product.android.daos;

import id.co.ptdmc.product.android.models.Comment;
import id.co.ptdmc.product.android.utils.CommonUtil;
import id.co.ptdmc.product.android.utils.DbDefinitionUtil;
import id.co.ptdmc.product.android.utils.SQLLiteUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

/**
 * Implementation DAO for {@link Comment} bean data model.
 *
 * @author Rochmat Santoso
 * */
public final class CommentDao extends BaseDao implements IDao<Comment> {

    /**
     * @param paramSqlLiteUtil the {@link SQLLiteUtil} object
     * @param paramApplicationContext the application context object
     * */
    public CommentDao(
            final SQLLiteUtil paramSqlLiteUtil,
            final Context paramApplicationContext) {
        super(paramSqlLiteUtil, paramApplicationContext);
    }

    @Override
    public List<Comment> selectByFilter(final Map<String, Object> filter) {
        String[] projection = {
                DbDefinitionUtil._ID,
                DbDefinitionUtil.COLUMN_COMMENT_TEXT,
                DbDefinitionUtil.COLUMN_COMMENT_DATE,
                DbDefinitionUtil.COLUMN_COMMENT_USER_NAME,
                DbDefinitionUtil.COLUMN_COMMENT_USER_PROFILE,
                DbDefinitionUtil.COLUMN_COMMENT_SOCMED_ID,
                DbDefinitionUtil.COLUMN_COMMENT_SOCMED_TYPE,
                DbDefinitionUtil.COLUMN_POST_ID,
                DbDefinitionUtil.COLUMN_COMMENT_SERVER_ID,
                DbDefinitionUtil.COLUMN_POST_SERVER_ID
            };

        String postId = null;
        String whereClause = null;
        String[] whereArgs = null;
        if (filter != null) {
            postId = (String) filter.get(DbDefinitionUtil.COLUMN_POST_ID);
        }
        if (postId != null) {
            whereClause = DbDefinitionUtil.COLUMN_POST_ID + " = ?";
            whereArgs = new String[] {postId};
        }

        String sortOrder = DbDefinitionUtil.COLUMN_COMMENT_DATE + " ASC";

        SQLiteDatabase db = getSqlLiteUtil().getWritableDatabase();
        Cursor cursor = db.query(
                DbDefinitionUtil.TABLE_COMMENT,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                sortOrder);

        List<Comment> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            String id = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil._ID));
            String text = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_COMMENT_TEXT));
            String date = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_COMMENT_DATE));
            String userName = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_COMMENT_USER_NAME));
            String userProfile = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_COMMENT_USER_PROFILE));
            String socmedId = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_COMMENT_SOCMED_ID));
            String socmedType = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_COMMENT_SOCMED_TYPE));
            postId = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_POST_ID));
            String commentServerId = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_COMMENT_SERVER_ID));
            String postServerId = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_POST_SERVER_ID));

            SimpleDateFormat dateFormat = CommonUtil.getDateFormatDB();

            Comment comment = new Comment();
            comment.setCommentId(id);
            comment.setCommentText(text);
            try {
                comment.setCommentDate(dateFormat.parse(date));
            } catch (ParseException e) {
                Toast.makeText(
                        getApplicationContext(),
                        "Error parsing comment date from database",
                        Toast.LENGTH_LONG).show();
            }
            comment.setCommentUserName(userName);
            comment.setCommentUserProfile(userProfile);
            comment.setCommentSocmedId(socmedId);
            comment.setCommentSocmedType(socmedType);
            comment.setPostId(postId);
            comment.setPostServerId(postServerId);
            comment.setCommentServerId(commentServerId);

            result.add(comment);
        }
        return result;
    }

    @Override
    public void insert(final Comment x) {
        SimpleDateFormat dateFormat = CommonUtil.getDateFormatDB();
        String date = dateFormat.format(x.getCommentDate());

        ContentValues values = new ContentValues();
        values.put(DbDefinitionUtil.COLUMN_COMMENT_TEXT, x.getCommentText());
        values.put(DbDefinitionUtil.COLUMN_COMMENT_DATE, date);
        values.put(
                DbDefinitionUtil.COLUMN_COMMENT_USER_NAME,
                x.getCommentUserName());
        values.put(
                DbDefinitionUtil.COLUMN_COMMENT_USER_PROFILE,
                x.getCommentUserProfile());
        values.put(
                DbDefinitionUtil.COLUMN_COMMENT_SOCMED_ID,
                x.getCommentSocmedId());
        values.put(
                DbDefinitionUtil.COLUMN_COMMENT_SOCMED_TYPE,
                x.getCommentSocmedType());
        values.put(DbDefinitionUtil.COLUMN_POST_ID, x.getPostId());
        values.put(DbDefinitionUtil.COLUMN_POST_SERVER_ID, x.getPostServerId());
        values.put(
                DbDefinitionUtil.COLUMN_COMMENT_SERVER_ID,
                x.getCommentServerId());

        SQLiteDatabase db = getSqlLiteUtil().getWritableDatabase();
        db.insert(DbDefinitionUtil.TABLE_COMMENT, null, values);
    }

    @Override
    public void update(final Comment x) {
        // not implemented yet
    }

    @Override
    public void delete(final Comment x) {
        // not implemented yet
    }

}
