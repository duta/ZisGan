/**
 * A package that contains Data Access Object classes.
 *
 * @author Rochmat Santoso
 */
package id.co.ptdmc.product.android.daos;
