package id.co.ptdmc.product.android.daos;

import id.co.ptdmc.product.android.models.Comment;
import id.co.ptdmc.product.android.utils.CommonUtil;
import id.co.ptdmc.product.android.utils.DbDefinitionUtil;
import id.co.ptdmc.product.android.utils.SQLLiteUtil;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Toast;

/**
 * Implementation DAO for {@link Comment} bean data model using Parse.com.
 *
 * @author Rochmat Santoso
 * */
public final class CommentParseDao extends BaseDao implements IDao<Comment> {

    /**
     * Parse tbl_post class.
     * */
    private static final String PARSE_COMMENT_CLASS = "https://api.parse.com/1/classes/tbl_comment";

    /**
     * Parse success response code.
     * */
    private static final int RESPONSE_CODE_SUCCESS = 200;

    /**
     * @param paramSqlLiteUtil the {@link SQLLiteUtil} object
     * @param paramApplicationContext the application context object
     * */
    public CommentParseDao(
            final SQLLiteUtil paramSqlLiteUtil,
            final Context paramApplicationContext) {
        super(paramSqlLiteUtil, paramApplicationContext);
    }

    @Override
    public List<Comment> selectByFilter(final Map<String, Object> filter) {
        String postId = null;
        if (filter != null) {
            postId = (String) filter.get(DbDefinitionUtil.COLUMN_POST_SERVER_ID);
        }

        List<Comment> result = new ArrayList<>();
        String strUrl = null;
        try {
            if (postId != null) {
                String whereClause = "where={\"post_id\":\"" + postId + "\"}";
                strUrl = "?" + URLEncoder.encode(whereClause, "UTF-8");
            }

            URL url = new URL(PARSE_COMMENT_CLASS + strUrl);

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("X-Parse-Application-Id", CommonUtil.PARSE_APP_ID);
            conn.setRequestProperty("X-Parse-REST-API-Key", CommonUtil.PARSE_REST_KEY);
            conn.setRequestProperty("Content-Type", CommonUtil.PARSE_CONTENT_TYPE);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject jsnobject = new JSONObject(response.toString());
            JSONArray jsonArray = jsnobject.getJSONArray("results");
            for (int i = 0; i < jsonArray.length(); i++) {
                SimpleDateFormat dateFormat = CommonUtil.getDateFormatDB();
                JSONObject obj = jsonArray.getJSONObject(i);
                Comment comment = new Comment();
                comment.setCommentServerId(obj.getString("objectId"));
                comment.setCommentText(obj.getString("comment_text"));
                try {
                    comment.setCommentDate(dateFormat.parse(obj.getString("comment_date")));
                } catch (ParseException e) {
                    Toast.makeText(
                            getApplicationContext(),
                            "Error parsing comment date from database",
                            Toast.LENGTH_LONG).show();
                }
                comment.setCommentUserName(obj.getString("comment_user_name"));
                comment.setCommentUserProfile(obj.getString("comment_user_profile"));
                comment.setCommentSocmedId(obj.getString("comment_socmed_id"));
                comment.setCommentSocmedType(obj.getString("comment_socmed_type"));
                comment.setPostServerId(obj.getString("post_id"));

                result.add(comment);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void insert(final Comment x) {
        // not implemented since it is done by batch
    }

    @Override
    public void update(final Comment x) {
        // not implemented
    }

    @Override
    public void delete(final Comment x) {
        // not implemented
    }

    /**
     * Insert a list of {@link Comment} to database by one trip batch.
     *
     * @param comments the list to save
     * */
    public void insertByBatch(final List<Comment> comments) {
        try {
            // create body
            JSONArray arrRequest = new JSONArray();
            for (Comment x : comments) {
                SimpleDateFormat dateFormat = CommonUtil.getDateFormatDB();
                String date = dateFormat.format(x.getCommentDate());

                JSONObject objBody = new JSONObject();
                objBody.put(DbDefinitionUtil.COLUMN_COMMENT_TEXT, x.getCommentText());
                objBody.put(DbDefinitionUtil.COLUMN_COMMENT_DATE, date);
                objBody.put(DbDefinitionUtil.COLUMN_COMMENT_USER_NAME, x.getCommentUserName());
                objBody.put(DbDefinitionUtil.COLUMN_COMMENT_USER_PROFILE, x.getCommentUserProfile());
                objBody.put(DbDefinitionUtil.COLUMN_COMMENT_SOCMED_ID, x.getCommentSocmedId());
                objBody.put(DbDefinitionUtil.COLUMN_COMMENT_SOCMED_TYPE, x.getCommentSocmedType());
                objBody.put(DbDefinitionUtil.COLUMN_POST_ID, x.getPostServerId());

                JSONObject params = new JSONObject();
                params.put("method", "POST");
                params.put("path", "/1/classes/tbl_comment");
                params.put("body", objBody);

                arrRequest.put(params);
            }

            // create connection
            URL obj = new URL("https://api.parse.com/1/batch");

            HttpsURLConnection conn = (HttpsURLConnection) obj.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("X-Parse-Application-Id", CommonUtil.PARSE_APP_ID);
            conn.setRequestProperty("X-Parse-REST-API-Key", CommonUtil.PARSE_REST_KEY);
            conn.setRequestProperty("Content-Type", CommonUtil.PARSE_CONTENT_TYPE);
            conn.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes("{\"requests\":" + arrRequest.toString() + "}");
            wr.flush();
            wr.close();

            // get response
            int responseCode = conn.getResponseCode();
            if (responseCode == RESPONSE_CODE_SUCCESS) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // save to local db
                JSONArray arrResponse = new JSONArray(response.toString());
                for (int i = 0; i < arrResponse.length(); i++) {
                    JSONObject objResponse = arrResponse.getJSONObject(i);
                    JSONObject objSuccess = objResponse.getJSONObject("success");

                    String objectId = objSuccess.getString("objectId");
                    Comment comment = comments.get(i);
                    comment.setCommentServerId(objectId);

                    ServiceDaoFactoryUtil.getInstance().getCommentDao().insert(comment);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
