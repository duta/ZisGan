package id.co.ptdmc.product.android.daos;

import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.CommonUtil;
import id.co.ptdmc.product.android.utils.DbDefinitionUtil;
import id.co.ptdmc.product.android.utils.SQLLiteUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

/**
 * Implementation DAO for {@link Post} bean data model.
 *
 * @author Rochmat Santoso
 * */
public final class PostDao extends BaseDao implements IDao<Post> {

    /**
     * @param paramSqlLiteUtil the {@link SQLLiteUtil} object
     * @param paramApplicationContext the application context object
     * */
    public PostDao(
            final SQLLiteUtil paramSqlLiteUtil,
            final Context paramApplicationContext) {
        super(paramSqlLiteUtil, paramApplicationContext);
    }

    @Override
    public List<Post> selectByFilter(final Map<String, Object> filter) {
        String[] projection = {
                DbDefinitionUtil._ID,
                DbDefinitionUtil.COLUMN_POST_IMAGE,
                DbDefinitionUtil.COLUMN_POST_TEXT,
                DbDefinitionUtil.COLUMN_POST_DATE,
                DbDefinitionUtil.COLUMN_POST_SOCMED_ID,
                DbDefinitionUtil.COLUMN_POST_SOCMED_TYPE,
                DbDefinitionUtil.COLUMN_POST_CURRENT_STATUS,
                DbDefinitionUtil.COLUMN_POST_SERVER_ID
            };

        String postSocmedId = null;
        String photoOrText = null;

        String whereClause = "1 = 1";
        String[] whereArgs = null;
        if (filter != null) {
            postSocmedId = (String) filter.get(DbDefinitionUtil.COLUMN_POST_SOCMED_ID);
            photoOrText = (String) filter.get("photoOrText");
        }
        if (postSocmedId != null) {
            whereClause = whereClause
                    + " and "
                    + DbDefinitionUtil.COLUMN_POST_SOCMED_ID
                    + " = ?";
            whereArgs = new String[] {postSocmedId};
        }
        if (photoOrText != null) {
            if (photoOrText.equals("text")) {
                whereClause = whereClause
                        + " and "
                        + DbDefinitionUtil.COLUMN_POST_IMAGE
                        + " is null";
            } else if (photoOrText.equals("photo")) {
                whereClause = whereClause
                        + " and "
                        + DbDefinitionUtil.COLUMN_POST_IMAGE
                        + " is not null";
            }
        }

        String sortOrder = DbDefinitionUtil.COLUMN_POST_DATE + " ASC";

        SQLiteDatabase db = getSqlLiteUtil().getWritableDatabase();
        Cursor cursor = db.query(
                DbDefinitionUtil.TABLE_POST,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                sortOrder);

        List<Post> result = new ArrayList<>();
        while (cursor.moveToNext()) {
            String id = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil._ID));
            String image = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_POST_IMAGE));
            String text = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_POST_TEXT));
            String date = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_POST_DATE));
            String socmedId = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_POST_SOCMED_ID));
            String socmedType = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_POST_SOCMED_TYPE));
            String currStatus = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_POST_CURRENT_STATUS));
            String serverId = cursor.getString(cursor.getColumnIndex(
                    DbDefinitionUtil.COLUMN_POST_SERVER_ID));

            SimpleDateFormat dateFormat = CommonUtil.getDateFormatDB();

            Post post = new Post();
            post.setPostId(id);
            post.setPostImage(image);
            post.setPostText(text);
            try {
                post.setPostCreationDate(dateFormat.parse(date));
            } catch (ParseException e) {
                Toast.makeText(
                        getApplicationContext(),
                        "Error parsing post date from database",
                        Toast.LENGTH_LONG).show();
            }
            post.setPostSocmedId(socmedId);
            post.setPostSocmedType(socmedType);
            post.setPostCurrStatus(currStatus);
            post.setPostServerId(serverId);

            result.add(post);
        }
        return result;
    }

    @Override
    public void insert(final Post x) {
        SimpleDateFormat dateFormat = CommonUtil.getDateFormatDBUTC();
        String date = dateFormat.format(x.getPostCreationDate());

        ContentValues values = new ContentValues();
        values.put(DbDefinitionUtil.COLUMN_POST_IMAGE, x.getPostImage());
        values.put(DbDefinitionUtil.COLUMN_POST_TEXT, x.getPostText());
        values.put(DbDefinitionUtil.COLUMN_POST_DATE, date);
        values.put(DbDefinitionUtil.COLUMN_POST_SOCMED_ID, x.getPostSocmedId());
        values.put(
                DbDefinitionUtil.COLUMN_POST_SOCMED_TYPE,
                x.getPostSocmedType());
        values.put(
                DbDefinitionUtil.COLUMN_POST_CURRENT_STATUS,
                x.getPostCurrStatus());
        values.put(DbDefinitionUtil.COLUMN_POST_SERVER_ID, x.getPostServerId());

        SQLiteDatabase db = getSqlLiteUtil().getWritableDatabase();
        db.insert(DbDefinitionUtil.TABLE_POST, null, values);
    }

    @Override
    public void update(final Post x) {
        ContentValues values = new ContentValues();
        values.put(
                DbDefinitionUtil.COLUMN_POST_CURRENT_STATUS,
                x.getPostCurrStatus());

        String whereClause = DbDefinitionUtil._ID + " = ?";
        String[] whereArgs = new String[] {x.getPostId()};

        SQLiteDatabase db = getSqlLiteUtil().getWritableDatabase();
        db.update(DbDefinitionUtil.TABLE_POST, values, whereClause, whereArgs);
    }

    @Override
    public void delete(final Post x) {
        // not implemented yet
    }

}
