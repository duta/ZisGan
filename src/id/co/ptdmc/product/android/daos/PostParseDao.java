package id.co.ptdmc.product.android.daos;

import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.CommonUtil;
import id.co.ptdmc.product.android.utils.DbDefinitionUtil;
import id.co.ptdmc.product.android.utils.SQLLiteUtil;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

/**
 * Implementation DAO for {@link Post} bean data model using Parse.com.
 *
 * @author Rochmat Santoso
 * */
public final class PostParseDao extends BaseDao implements IDao<Post> {

    /**
     * Parse tbl_post class.
     * */
    private static final String PARSE_POST_CLASS = "https://api.parse.com/1/classes/tbl_post";

    /**
     * Parse success response code.
     * */
    private static final int RESPONSE_CODE_SUCCESS = 201;

    /**
     * @param paramSqlLiteUtil the {@link SQLLiteUtil} object
     * @param paramApplicationContext the application context object
     * */
    public PostParseDao(
            final SQLLiteUtil paramSqlLiteUtil,
            final Context paramApplicationContext) {
        super(paramSqlLiteUtil, paramApplicationContext);
    }

    @Override
    public List<Post> selectByFilter(final Map<String, Object> filter) {
        String text = null;
        String postUser = null;
        Integer skip = 0;
        if (filter != null) {
            text = (String) filter.get(DbDefinitionUtil.COLUMN_POST_TEXT);
            postUser = (String) filter.get("post_user_id");
            skip = (Integer) filter.get("skip");
        }
        List<Post> result = new ArrayList<>();
        String strUrl = null;
        try {
            if (text != null) {
                text = text.toLowerCase(Locale.getDefault());
                String[] arr = text.split("\\s+");

                JSONObject objAll = new JSONObject();
                objAll.put("$all", new JSONArray(Arrays.asList(arr)));

                JSONObject objNull = new JSONObject();
                objNull.put("$nin", new JSONArray(Arrays.asList(new Object[] {false, JSONObject.NULL})));

                JSONObject objTags = new JSONObject();
                objTags.put("post_search_tags", objAll);
                objTags.put(DbDefinitionUtil.COLUMN_POST_IMAGE, objNull);

                strUrl = "?where=" + URLEncoder.encode(objTags.toString(), "UTF-8") + "&limit=1000&skip=" + skip;
            }
            if (postUser != null) {
                JSONObject objUser = new JSONObject();
                objUser.put("post_user_id", postUser);

                strUrl = "?where=" + URLEncoder.encode(objUser.toString(), "UTF-8") + "&limit=1000";
            }
            URL url = new URL(PARSE_POST_CLASS + strUrl);

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("X-Parse-Application-Id", CommonUtil.PARSE_APP_ID);
            conn.setRequestProperty("X-Parse-REST-API-Key", CommonUtil.PARSE_REST_KEY);
            conn.setRequestProperty("Content-Type", CommonUtil.PARSE_CONTENT_TYPE);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject jsnobject = new JSONObject(response.toString());
            JSONArray jsonArray = jsnobject.getJSONArray("results");
            for (int i = 0; i < jsonArray.length(); i++) {
                SimpleDateFormat dateFormat = CommonUtil.getDateFormatDB();
                JSONObject obj = jsonArray.getJSONObject(i);
                Post post = new Post();
                post.setPostServerId(obj.getString("objectId"));
                if (obj.has("post_image")) {
                    post.setPostImage(obj.getString("post_image"));
                }
                post.setPostText(obj.getString("post_text"));
                try {
                    post.setPostCreationDate(dateFormat.parse(obj.getString("post_date")));
                } catch (ParseException e) {
                    Toast.makeText(
                            getApplicationContext(),
                            "Error parsing post date from database",
                            Toast.LENGTH_LONG).show();
                }
                post.setPostSocmedId(obj.getString("post_socmed_id"));
                post.setPostSocmedType(obj.getString("post_socmed_type"));

                result.add(post);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void insert(final Post x) {
        try {
            URL obj = new URL(PARSE_POST_CLASS);

            HttpsURLConnection conn = (HttpsURLConnection) obj.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("X-Parse-Application-Id", CommonUtil.PARSE_APP_ID);
            conn.setRequestProperty("X-Parse-REST-API-Key", CommonUtil.PARSE_REST_KEY);
            conn.setRequestProperty("Content-Type", CommonUtil.PARSE_CONTENT_TYPE);
            conn.setDoOutput(true);

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            SimpleDateFormat dateFormat = CommonUtil.getDateFormatDBUTC();
            String date = dateFormat.format(cal.getTime());

            JSONObject params = new JSONObject();
            params.put(DbDefinitionUtil.COLUMN_POST_IMAGE, x.getPostImage());
            params.put(DbDefinitionUtil.COLUMN_POST_TEXT, x.getPostText());
            params.put(DbDefinitionUtil.COLUMN_POST_DATE, date);
            params.put(DbDefinitionUtil.COLUMN_POST_SOCMED_ID, x.getPostSocmedId());
            params.put(DbDefinitionUtil.COLUMN_POST_SOCMED_TYPE, x.getPostSocmedType());

            String text = x.getPostText();
            if (text != null) {
                text = text.toLowerCase(Locale.getDefault());
                String[] arr = text.split("\\s+");
                params.put("post_search_tags", new JSONArray(Arrays.asList(arr)));
            }

            SharedPreferences pref = getApplicationContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
            String fbUserId = pref.getString("fbUserId", "");
            params.put("post_user_id", fbUserId);

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();

            int responseCode = conn.getResponseCode();
            if (responseCode == RESPONSE_CODE_SUCCESS) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // save to local db
                JSONObject responseObj = new JSONObject(response.toString());
                String postServerId = responseObj.getString("objectId");
                x.setPostServerId(postServerId);
                x.setPostCreationDate(cal.getTime());
                ServiceDaoFactoryUtil.getInstance().getPostDao().insert(x);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(final Post x) {
        // not implemented
    }

    @Override
    public void delete(final Post x) {
        // not implemented
    }

}
