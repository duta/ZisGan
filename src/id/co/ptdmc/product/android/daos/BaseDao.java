package id.co.ptdmc.product.android.daos;

import android.content.Context;
import id.co.ptdmc.product.android.utils.SQLLiteUtil;

/**
 * Base class for all DAO classes.
 *
 * @author Rochmat Santoso
 * */
public class BaseDao {

    /**
     * SQL Lite utility object.
     * */
    private SQLLiteUtil sqlLiteUtil;

    /**
     * Application context object.
     * */
    private Context applicationContext;

    /**
     * @param paramSqlLiteUtil the {@link SQLLiteUtil} object
     * @param paramApplicationContext the application context object
     * */
    public BaseDao(
            final SQLLiteUtil paramSqlLiteUtil,
            final Context paramApplicationContext) {
        super();
        this.sqlLiteUtil = paramSqlLiteUtil;
        this.applicationContext = paramApplicationContext;
    }

    /**
     * @return the sqlLiteUtil
     */
    public final SQLLiteUtil getSqlLiteUtil() {
        return sqlLiteUtil;
    }

    /**
     * @param paramSqlLiteUtil the sqlLiteUtil to set
     */
    public final void setSqlLiteUtil(final SQLLiteUtil paramSqlLiteUtil) {
        this.sqlLiteUtil = paramSqlLiteUtil;
    }

    /**
     * @return the applicationContext
     */
    public final Context getApplicationContext() {
        return applicationContext;
    }

    /**
     * @param paramApplicationContext the applicationContext to set
     */
    public final void setApplicationContext(
            final Context paramApplicationContext) {
        this.applicationContext = paramApplicationContext;
    }

}
