package id.co.ptdmc.product.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/**
 * Login instagram activity class.
 *
 * @author Rochmat Santoso
 * */
public final class LoginInstagramActivity extends ActionBarActivity {

    /**
     * Instagram's URL to authenticate user.
     * */
    private static final String INSTAGRAM_AUTH_URL = "https://instagram.com/oauth/authorize/";

    /**
     * Our URL that is used by Instagram to redirect authentication process.
     * */
    private static final String INSTAGRAM_REDIRECT_URI = "http://www.ptdmc.co.id/";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        String clientId = getString(R.string.instagram_client_id);
        String authURLStr = INSTAGRAM_AUTH_URL + "?client_id=" + clientId
                + "&redirect_uri=" + INSTAGRAM_REDIRECT_URI
                + "&response_type=token";

        WebView webLogin = (WebView) findViewById(R.id.webLogin);
        webLogin.getSettings().setJavaScriptEnabled(true);
        webLogin.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(
                    final WebView view, final String url) {
                if (url.startsWith(INSTAGRAM_REDIRECT_URI)) {
                    if (url.contains("#access_token=")) { // user authorized
                        String[] split = url.split("#access_token=");
                        String token = split[1];
                        Log.d("Instagram URL", url);
                        Log.d("Instagram Token", token);

                        // save token to shared preferences
                        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("instagramToken", token);
                        editor.commit();
                    } else if (url.contains("error=access_denied")) { // user refuses to authorize
                        Toast.makeText(
                                getApplicationContext(),
                                "Access denied",
                                Toast.LENGTH_LONG).show();
                    }
                    finish();
                    return true;
                }
                return false;
            }
        });
        webLogin.loadUrl(authURLStr);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Intent intent = null;
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static final class PlaceholderFragment extends Fragment {
        @Override
        public View onCreateView(
                final LayoutInflater inflater,
                final ViewGroup container,
                final Bundle savedInstanceState) {
            View rootView = inflater.inflate(
                    R.layout.fragment_login_instagram, container, false);
            return rootView;
        }
    }

}
