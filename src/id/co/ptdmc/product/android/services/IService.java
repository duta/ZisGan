package id.co.ptdmc.product.android.services;

import java.io.File;
import java.util.Map;

/**
 * Base service for all service classes.
 *
 * @author Rochmat Santoso
 * */
public interface IService {

    /**
     * Post a text to social media.
     *
     * @param text text to post
     * @return result string
     * */
    String postText(String text);

    /**
     * Post an image to social media.
     *
     * @param image the image to post
     * @param caption the caption of the image
     * @return result string
     * */
    String postImage(File image, String caption);

    /**
     * Get comments on social media by filter and update local data.
     *
     * @param filter a map that contains filter to search comments
     * @return result string
     * */
    String updateComments(Map<String , Object> filter);

    /**
     * Post a comment on a post using post id.
     *
     * @param postId the post id where the comment will be attached
     * @param postSocmedId the post social media id where the comment will be
     *        attached
     * @param comment the comment to post
     * @return result string
     * */
    String postComment(String postId, String postSocmedId, String comment);

}
