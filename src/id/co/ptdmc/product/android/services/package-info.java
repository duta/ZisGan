/**
 * A package that contains service classes.
 *
 * @author Rochmat Santoso
 */
package id.co.ptdmc.product.android.services;
