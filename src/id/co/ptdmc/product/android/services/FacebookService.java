package id.co.ptdmc.product.android.services;

import id.co.ptdmc.product.android.models.Comment;
import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.CommonUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.Response;
import com.facebook.Session;

/**
 * Implementation service for Facebook.
 *
 * @author Rochmat Santoso
 * */
public final class FacebookService extends BaseService {

    /**
     * Facebook batch success response code.
     * */
    private static final int RESPONSE_CODE_SUCCESS = 200;

    /**
     * @param paramActivity the activity that calls this service
     * */
    public FacebookService(final Context paramActivity) {
        super(paramActivity);
    }

    @Override
    public String postText(final String text) {
        Session session = Session.getActiveSession();
        Request request = Request.newStatusUpdateRequest(session, text, null);
        Response response = request.executeAndWait();

        FacebookRequestError error = response.getError();
        if (error != null) {
            return "Facebook error: " + error.getErrorMessage();
        }

        String postId = response.getGraphObject().getProperty("id").toString();

        // save to server
        Post post = new Post();
        post.setPostText(text);
        post.setPostSocmedId(postId);
        post.setPostSocmedType(CommonUtil.SOCMED_TYPE_FACEBOOK);

        getPostParseDao().insert(post);

        return "Status has been posted to Facebook";
    }

    @Override
    public String postImage(final File image, final String caption) {
        try {
            Session session = Session.getActiveSession();
            Request request = newUploadPhotoRequest(session, image, caption, null);
            Response response = request.executeAndWait();

            FacebookRequestError error = response.getError();
            if (error != null) {
                return "Facebook error: " + error.getErrorMessage();
            }

            String postId = response.getGraphObject().getProperty("id").toString();

            // save to server
            Post post = new Post();
            post.setPostText(caption);
            post.setPostImage(image.getName());
            post.setPostSocmedId(postId);
            post.setPostSocmedType(CommonUtil.SOCMED_TYPE_FACEBOOK);

            getPostParseDao().insert(post);

            return "Image has been posted to Facebook";
        } catch (FileNotFoundException e) {
            return "Facebook error: File not found";
        }
    }

    @Override
    public String updateComments(final Map<String, Object> filter) {
        String result = null;
        try {
            // get all local posts from database
            List<Post> posts = getPostDao().selectByFilter(null);

            // create body
            JSONArray arrRequest = new JSONArray();
            for (Post x : posts) {
                String url = "/" + x.getPostSocmedId() + "/comments";

                JSONObject params = new JSONObject();
                params.put("method", "GET");
                params.put("relative_url", url);

                arrRequest.put(params);
            }

            // create connection
            URL obj = new URL("https://graph.facebook.com");

            HttpsURLConnection conn = (HttpsURLConnection) obj.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            StringBuilder builder = new StringBuilder();
            builder.append("access_token=");
            builder.append(Session.getActiveSession().getAccessToken());
            builder.append("&batch=");
            builder.append(arrRequest.toString());

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(builder.toString());
            wr.flush();
            wr.close();

            // get response
            int responseCode = conn.getResponseCode();
            if (responseCode == RESPONSE_CODE_SUCCESS) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // get all comments
                List<Comment> comments = getCommentDao().selectByFilter(null);
                HashMap<String, Comment> commentIds = new HashMap<>();
                for (Comment comment : comments) {
                    commentIds.put(comment.getCommentSocmedId(), comment);
                }

                List<Comment> commentsToSave = new ArrayList<>();
                SimpleDateFormat dateFormatFB = CommonUtil.getDateFormatFB();

                // save to db
                JSONArray arrResponse = new JSONArray(response.toString());
                for (int i = 0; i < arrResponse.length(); i++) {
                    boolean isUpdate = false;
                    Post post = posts.get(i);
                    JSONObject objResponse = arrResponse.getJSONObject(i);
                    String body = objResponse.getString("body");
                    JSONObject objBody = new JSONObject(body);
                    JSONArray arrData = objBody.getJSONArray("data");
                    for (int j = 0; j < arrData.length(); j++) {
                        JSONObject objComment = arrData.getJSONObject(j);
                        String commentId = objComment.getString("id");
                        String commentMsg = objComment.getString("message");
                        String commentDate = objComment.getString("created_time");
                        JSONObject fromObj = objComment.getJSONObject("from");
                        String userName = fromObj.getString("name");
                        String userProfile = fromObj.getString("id");

                        if (!commentIds.containsKey(commentId)) {
                            isUpdate = true;
                            Date strToDate = dateFormatFB.parse(commentDate);

                            Comment newComment = new Comment();
                            newComment.setCommentText(commentMsg);
                            newComment.setCommentDate(strToDate);
                            newComment.setCommentUserName(userName);
                            newComment.setCommentUserProfile(userProfile);
                            newComment.setCommentSocmedId(commentId);
                            newComment.setCommentSocmedType(CommonUtil.SOCMED_TYPE_FACEBOOK);
                            newComment.setPostId(post.getPostId());
                            newComment.setPostServerId(post.getPostServerId());

                            commentsToSave.add(newComment);
                        }
                    }

                    if (isUpdate) {
                        Post postObj = new Post();
                        postObj.setPostId(post.getPostId());
                        postObj.setPostCurrStatus("New Update");
                        getPostDao().update(postObj);

                        result = "New Update";
                    }
                }
                getCommentParseDao().insertByBatch(commentsToSave);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String postComment(
            final String postId,
            final String postSocmedId,
            final String comment) {
        Bundle params = new Bundle();
        params.putString("message", comment);

        Session session = Session.getActiveSession();
        Request request = new Request(session, "/" + postSocmedId + "/comments", params, HttpMethod.POST, null);
        Response response = request.executeAndWait();

        FacebookRequestError error = response.getError();
        if (error != null) {
            return "Facebook error: " + error.getErrorMessage();
        }

        // remark these since these can be retrieved through synchronization
        // and cause double saving
        /*String commentId = response.getGraphObject().getProperty("id").toString();

        Comment commentObj = new Comment();
        commentObj.setPostId(postId);
        commentObj.setCommentText(comment);
        commentObj.setCommentSocmedId(commentId);
        commentObj.setCommentSocmedType(CommonUtil.SOCMED_TYPE_FACEBOOK);
        commentObj.setCommentDate(new Date());

        getCommentDao().insert(commentObj);*/

        return "Comment has been posted to Facebook";
    }

    /**
     * A new method to upload image and its caption since facebook doesn't
     * provide one.
     *
     * @param session the session to make request
     * @param file the image file to upload
     * @param caption image's caption
     * @param callback callback to call after request
     * @return the Request object
     * @throws FileNotFoundException thrown if the image file is not found
     * */
    private Request newUploadPhotoRequest(
            final Session session,
            final File file,
            final String caption,
            final Callback callback) throws FileNotFoundException {
        ParcelFileDescriptor descriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
        Bundle parameters = new Bundle(3);
        parameters.putParcelable("picture", descriptor);
        parameters.putString("caption", caption);

        return new Request(session, "me/photos", parameters, HttpMethod.POST, callback);
    }

}
