package id.co.ptdmc.product.android.services;

import java.io.File;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

/**
 * Implementation service for Instagram.
 *
 * @author Rochmat Santoso
 * */
public final class InstagramService extends BaseService {

    /**
     * @param paramActivity the activity that calls this service
     * */
    public InstagramService(final Context paramActivity) {
        super(paramActivity);
    }

    @Override
    public String postText(final String text) {
        return null;
    }

    @Override
    public String postImage(final File image, final String caption) {
        if (!isInstagramInstalled()) {
            return null;
        }
        Uri uri = Uri.fromFile(image);

        Intent instagram = new Intent(android.content.Intent.ACTION_SEND);
        instagram.setType("image/*");
        instagram.putExtra(Intent.EXTRA_STREAM, uri);
        instagram.putExtra(Intent.EXTRA_TEXT, caption);
        instagram.setPackage("com.instagram.android");
        instagram.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivity(instagram);

        return null;
    }

    @Override
    public String updateComments(final Map<String, Object> filter) {
        return null;
    }

    @Override
    public String postComment(
            final String postId,
            final String postSocmedId,
            final String comment) {
        return null;
    }

    /**
     * Check if instagram is installed.
     *
     * @return true false value
     * */
    private boolean isInstagramInstalled() {
        PackageManager packageManager = getActivity().getPackageManager();
        boolean result = false;
        try {
            packageManager.getPackageInfo(
                    "com.instagram.android", PackageManager.GET_ACTIVITIES);
            result = true;
        } catch (PackageManager.NameNotFoundException e) {
            result = false;
        }
        return result;
    }

}
