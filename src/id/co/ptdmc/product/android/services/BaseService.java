package id.co.ptdmc.product.android.services;

import id.co.ptdmc.product.android.daos.CommentDao;
import id.co.ptdmc.product.android.daos.CommentParseDao;
import id.co.ptdmc.product.android.daos.PostDao;
import id.co.ptdmc.product.android.daos.PostParseDao;
import android.content.Context;

/**
 * Base class for all service classes.
 *
 * @author Rochmat Santoso
 * */
public abstract class BaseService implements IService {

    /**
     * Activity that calls this service.
     * */
    private Context activity;

    /**
     * PostDao object.
     * */
    private PostDao postDao;

    /**
     * CommentDao object.
     * */
    private CommentDao commentDao;

    /**
     * PostParseDao object.
     * */
    private PostParseDao postParseDao;

    /**
     * CommentParseDao object.
     * */
    private CommentParseDao commentParseDao;

    /**
     * @param paramActivity the activity that calls this service
     * */
    public BaseService(final Context paramActivity) {
        this.activity = paramActivity;
    }

    /**
     * @return the activity
     */
    public final Context getActivity() {
        return activity;
    }

    /**
     * @param paramActivity the activity to set
     */
    public final void setActivity(final Context paramActivity) {
        this.activity = paramActivity;
    }

    /**
     * @return the postDao
     */
    public final PostDao getPostDao() {
        return postDao;
    }

    /**
     * @param paramPostDao the postDao to set
     */
    public final void setPostDao(final PostDao paramPostDao) {
        this.postDao = paramPostDao;
    }

    /**
     * @return the commentDao
     */
    public final CommentDao getCommentDao() {
        return commentDao;
    }

    /**
     * @param paramCommentDao the commentDao to set
     */
    public final void setCommentDao(final CommentDao paramCommentDao) {
        this.commentDao = paramCommentDao;
    }

    /**
     * @return the postParseDao
     */
    public final PostParseDao getPostParseDao() {
        return postParseDao;
    }

    /**
     * @param paramPostParseDao the postParseDao to set
     */
    public final void setPostParseDao(final PostParseDao paramPostParseDao) {
        this.postParseDao = paramPostParseDao;
    }

    /**
     * @return the commentParseDao
     */
    public final CommentParseDao getCommentParseDao() {
        return commentParseDao;
    }

    /**
     * @param paramCommentParseDao the commentParseDao to set
     */
    public final void setCommentParseDao(
            final CommentParseDao paramCommentParseDao) {
        this.commentParseDao = paramCommentParseDao;
    }

}
