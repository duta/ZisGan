package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.CommonUtil;

import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

/**
 * An adapter class for displaying a list of thumbnail image.
 *
 * @author Rochmat Santoso
 * */
public final class MainPhotoContentAdapter extends ArrayAdapter<Map<String, Post>> {

    /**
     * Application context.
     * */
    private Context context;

    /**
     * List of thumbnail image.
     * */
    private List<Map<String, Post>> data;

    /**
     * @param ctx application context
     * @param objects the data to display
     * */
    public MainPhotoContentAdapter(
            final Context ctx, final List<Map<String, Post>> objects) {
        super(ctx, R.layout.fragment_main_photo_content, objects);

        this.context = ctx;
        this.data = objects;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(
            final int position, View convertView, final ViewGroup parent) {
        float size = calculateViewSize();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_main_photo_content, null);

            ViewHolder holder = new ViewHolder();
            holder.imgView1 = (ImageView) convertView.findViewById(R.id.imgView1);
            holder.imgView2 = (ImageView) convertView.findViewById(R.id.imgView2);
            holder.imgView3 = (ImageView) convertView.findViewById(R.id.imgView3);

            holder.imgView1.getLayoutParams().height = (int) size;
            holder.imgView1.getLayoutParams().width = (int) size;
            holder.imgView2.getLayoutParams().height = (int) size;
            holder.imgView2.getLayoutParams().width = (int) size;
            holder.imgView3.getLayoutParams().height = (int) size;
            holder.imgView3.getLayoutParams().width = (int) size;

            convertView.setTag(holder);
        }

        Map<String, Post> obj = data.get(position);
        final Post post1 = obj.get("post1");
        final Post post2 = obj.get("post2");
        final Post post3 = obj.get("post3");

        ViewHolder holder = (ViewHolder) convertView.getTag();
        if (holder.task1 != null) {
            holder.task1.cancel(true);
        }
        if (holder.task2 != null) {
            holder.task2.cancel(true);
        }
        if (holder.task3 != null) {
            holder.task3.cancel(true);
        }
        displayImage(post1, holder.imgView1, size, 1, holder);
        displayImage(post2, holder.imgView2, size, 2, holder);
        displayImage(post3, holder.imgView3, size, 3, holder);

        return convertView;
    }

    /**
     * Display image in ImageView after scaling.
     *
     * @param post the post that contains image
     * @param imgView the ImageView
     * @param displaySize desired height and width of the image
     * @param number number of image in order
     * @param holder the ViewHolder object
     * */
    private void displayImage(
            final Post post,
            final ImageView imgView,
            final float displaySize,
            final int number,
            final ViewHolder holder) {
        // reset ImageView
        imgView.setImageBitmap(null);
        imgView.setOnClickListener(null);

        // display post's image
        if (post != null) {
            if (post.getPostCurrStatus() != null) {
                imgView.setBackgroundResource(R.drawable.border);
            } else {
                imgView.setBackgroundResource(0);
            }
            AsyncTask<Post, Integer, Bitmap> task = new AsyncTask<Post, Integer, Bitmap>() {
                @Override
                protected Bitmap doInBackground(final Post... params) {
                    Bitmap bitmap = CommonUtil.scaleAndDecodeBitmap(
                            CommonUtil.getLocalPath() + post.getPostImage(),
                            displaySize,
                            displaySize);
                    return bitmap;
                }

                @Override
                protected void onPostExecute(final Bitmap bitmap) {
                    imgView.setImageBitmap(bitmap);
                    imgView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            if (post.getPostCurrStatus() != null) {
                                imgView.setBackgroundResource(R.drawable.border_zero);
                            }
                            ((MainActivity) context).viewDetail(post);
                        }
                    });
                }
            };
            if (number == 1) {
                holder.task1 = task;
            } else if (number == 2) {
                holder.task2 = task;
            } else if (number == 3) {
                holder.task3 = task;
            }

            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                task.execute();
            }
        }
    }

    /**
     * Calculate display size.
     *
     * @return the display size
     * */
    private float calculateViewSize() {
        // calculate screen size
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        // since we have three ImageView in a row, divide by 3
        return displayMetrics.widthPixels / 3;
    }

    /**
     * A helper class that contains widgets to display thumbnail image.
     * */
    static class ViewHolder {
        /**
         * The first {@link ImageView}.
         * */
        private ImageView imgView1;

        /**
         * The second {@link ImageView}.
         * */
        private ImageView imgView2;

        /**
         * The third {@link ImageView}.
         * */
        private ImageView imgView3;

        /**
         * The task to load first image.
         * */
        private AsyncTask<Post, Integer, Bitmap> task1;

        /**
         * The task to load second image.
         * */
        private AsyncTask<Post, Integer, Bitmap> task2;

        /**
         * The task to load third image.
         * */
        private AsyncTask<Post, Integer, Bitmap> task3;
    }

}
