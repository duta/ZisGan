package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.components.QuickReturnFrameLayout;
import id.co.ptdmc.product.android.daos.PostDao;
import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.CommonUtil;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Fragment class that contains photo layout and handles its interactions.
 *
 * @author Rochmat Santoso
 * */
public final class MainPhotoFragment extends Fragment implements OnRefreshListener {

    /**
     * A static code that indicates a camera request.
     * */
    private static final int IMAGE_CAPTURE_REQUEST = 667;

    /**
     * A static code that indicates a media request.
     * */
    private static final int IMAGE_MEDIA_REQUEST = 668;

    /**
     * Path that holds current image taken by camera or media.
     * */
    private String currentPhotoPath;

    /**
     * SwipeRefreshLayout object.
     * */
    private SwipeRefreshLayout swipeRefreshPhoto;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        // attach quick return menus
        ListView listData = (ListView) getActivity().findViewById(R.id.listData);
        listData.setEmptyView(getActivity().findViewById(R.id.emptyPhoto));
        ((QuickReturnFrameLayout) getActivity().findViewById(R.id.frame)).attach(listData);

        swipeRefreshPhoto = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeRefreshPhoto);
        swipeRefreshPhoto.setOnRefreshListener(this);
        swipeRefreshPhoto.setColorSchemeResources(
                R.color.red, R.color.orange, R.color.green, R.color.blue);

        // populate list with data
        populateData(false);
    }

    @Override
    public View onCreateView(
            final LayoutInflater inflater,
            final ViewGroup container,
            final Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_main_photo, container, false);
        return rootView;
    }

    @Override
    public void onActivityResult(
            final int requestCode, final int resultCode, final Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IMAGE_CAPTURE_REQUEST) {
                Intent intent = new Intent(getActivity(), NewPostPhotoActivity.class);
                intent.putExtra("filePath", this.currentPhotoPath);
                startActivity(intent);
            } else if (requestCode == IMAGE_MEDIA_REQUEST) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                this.currentPhotoPath = cursor.getString(columnIndex);
                cursor.close();

                Intent intent = new Intent(getActivity(), NewPostPhotoActivity.class);
                intent.putExtra("filePath", this.currentPhotoPath);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onRefresh() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            new UpdateTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new UpdateTask().execute();
        }
    }

    /**
     * Populate data to display.
     *
     * @param forceRepopulate force {@link ListView} to be re-populated
     * */
    public void populateData(final boolean forceRepopulate) {
        ListView listData = (ListView) getActivity().findViewById(R.id.listData);
        ListAdapter adapter = listData.getAdapter();
        if (adapter == null || forceRepopulate) {
            List<Map<String, Post>> result = new ArrayList<>();

            Map<String, Object> filter = new HashMap<>();
            filter.put("photoOrText", "photo");
            PostDao dao = ServiceDaoFactoryUtil.getInstance().getPostDao();
            List<Post> posts = dao.selectByFilter(filter);
            for (int i = 0; i < posts.size(); i = i + 3) {
                Post post = posts.get(i);
                Map<String, Post> map = new HashMap<>();
                map.put("post1", post);

                if (i + 1 < posts.size()) {
                    post = posts.get(i + 1);
                    map.put("post2", post);
                }
                if (i + 2 < posts.size()) {
                    post = posts.get(i + 2);
                    map.put("post3", post);
                }
                result.add(map);
            }
            if (adapter == null) {
                adapter = new MainPhotoContentAdapter(getActivity(), result);
                listData.setAdapter(adapter);
            } else {
                ((MainPhotoContentAdapter) adapter).clear();
                ((MainPhotoContentAdapter) adapter).addAll(result);
            }
        }
    }

    /**
     * View detail activity.
     *
     * @param post the post object required to view the details
     * */
    public void viewDetail(final Post post) {
        Intent intent = new Intent(getActivity(), DetailPhotoActivity.class);
        intent.putExtra("post", post);
        startActivity(intent);
    }

    /**
     * Get a picture from camera.
     *
     * @param view the view
     * */
    public void takePicture(final View view) {
        File file = null;
        try {
            file = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> list = getActivity().getPackageManager().queryIntentActivities(
                intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (!list.isEmpty() && file != null) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
            startActivityForResult(intent, IMAGE_CAPTURE_REQUEST);
        }
    }

    /**
     * Choose an image from media.
     *
     * @param view the view
     * */
    public void chooseImage(final View view) {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        List<ResolveInfo> list = getActivity().getPackageManager().queryIntentActivities(
                intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (!list.isEmpty()) {
            startActivityForResult(intent, IMAGE_MEDIA_REQUEST);
        }
    }

    /**
     * Create a temporary image to be displayed on an ImageView.
     *
     * @return the image file
     * @throws IOException thrown if an error occurs during file creation
     * */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = CommonUtil.getDateFormatFileName().format(new Date());
        String imageFileName = "ZisGan_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",         /* suffix */
            storageDir      /* directory */
        );
        this.currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * Background task that update comments.
     * */
    private class UpdateTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(final Void... params) {
            ServiceDaoFactoryUtil util = ServiceDaoFactoryUtil.getInstance();
            String result = util.getFacebookService().updateComments(null);
            util.getInstagramService().updateComments(null);
            return result;
        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null && result.equals("New Update")) {
                populateData(true);
            }
            swipeRefreshPhoto.setRefreshing(false);
        }
    }

}
