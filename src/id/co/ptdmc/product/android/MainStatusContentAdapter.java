package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.CommonUtil;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * An adapter class for displaying a list of status.
 *
 * @author Rochmat Santoso
 * */
public final class MainStatusContentAdapter extends ArrayAdapter<Post> {

    /**
     * Application context.
     * */
    private Context context;

    /**
     * List of status.
     * */
    private List<Post> data;

    /**
     * @param ctx application context
     * @param objects the data to display
     * */
    public MainStatusContentAdapter(
            final Context ctx, final List<Post> objects) {
        super(ctx, R.layout.fragment_main_status_content, objects);

        this.context = ctx;
        this.data = objects;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(
            final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_main_status_content, null);

            ViewHolder holder = new ViewHolder();
            holder.txtStatus = (TextView) convertView.findViewById(R.id.txtStatus);
            holder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);

            convertView.setTag(holder);
        }

        Post obj = data.get(position);

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.txtStatus.setText(obj.getPostText());
        holder.txtDate.setText(CommonUtil.formatDateInFriendlyFormat(obj.getPostCreationDate()));

        return convertView;
    }

    /**
     * A helper class that contains widgets to display status.
     * */
    static class ViewHolder {
        /**
         * The status text.
         * */
        private TextView txtStatus;

        /**
         * The posting date text.
         * */
        private TextView txtDate;
    }

}
