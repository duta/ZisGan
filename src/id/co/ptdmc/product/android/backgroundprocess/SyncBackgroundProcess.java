package id.co.ptdmc.product.android.backgroundprocess;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.Session;

import id.co.ptdmc.product.android.R;
import id.co.ptdmc.product.android.daos.PostDao;
import id.co.ptdmc.product.android.daos.PostParseDao;
import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.CommonUtil;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

/**
 * Background process that handles local and server database synchronization.
 *
 * @author Rochmat Santoso
 * */
public final class SyncBackgroundProcess extends IntentService {

    /**
     * Application-specific action for this background process.
     * */
    public static final String ACTION_SYNC = "id.co.ptdmc.product.android.backgroundprocess.ACTION_SYNC";

    /**
     * Notification id number.
     * */
    private static final int SYNC_NOTIF_ID = 18;

    /**
     * Facebook batch success response code.
     * */
    private static final int RESPONSE_CODE_SUCCESS = 200;

    /**
     * Handler to display Toast's text.
     * */
    private Handler handler;

    /**
     * Default constructor.
     * */
    public SyncBackgroundProcess() {
        super("id.co.ptdmc.product.android.backgroundprocess.SyncBackgroundProcess");
        this.handler = new Handler();
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        // create notification
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("Synchronizing data...")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setTicker("Synchronizing data...");
        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.notify(SYNC_NOTIF_ID, notification.build());

        // get facebook user id
        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        String fbUserId = pref.getString("fbUserId", "");

        // insert posts to local database
        Map<String, Object> filter = new HashMap<>();
        filter.put("post_user_id", fbUserId);

        ServiceDaoFactoryUtil util = ServiceDaoFactoryUtil.getInstance();
        PostDao postDao = util.getPostDao();
        PostParseDao parseDao = util.getPostParseDao();
        List<Post> posts = parseDao.selectByFilter(filter);
        for (Post post : posts) {
            postDao.insert(post);
        }

        // get post images from facebook to download
        try {
            JSONArray arrRequest = new JSONArray();
            List<Post> postsToRemove = new ArrayList<>();
            for (Post post : posts) {
                if (post.getPostImage() == null) {
                    postsToRemove.add(post);
                    continue;
                }
                // create body to request images to facebook
                String relativeUrl = "/" + post.getPostSocmedId() + "/?fields=source,picture";

                JSONObject obj = new JSONObject();
                obj.put("method", "GET");
                obj.put("relative_url", relativeUrl);

                arrRequest.put(obj);
            }
            posts.removeAll(postsToRemove);

            // create connection
            URL url = new URL("https://graph.facebook.com");

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            StringBuilder builder = new StringBuilder();
            builder.append("access_token=");
            builder.append(Session.getActiveSession().getAccessToken());
            builder.append("&batch=");
            builder.append(arrRequest.toString());

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(builder.toString());
            wr.flush();
            wr.close();

            // get response
            int responseCode = conn.getResponseCode();
            if (responseCode == RESPONSE_CODE_SUCCESS) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // iterate response to download images
                JSONArray arrResponse = new JSONArray(response.toString());
                for (int i = 0; i < arrResponse.length(); i++) {
                    JSONObject objResponse = arrResponse.getJSONObject(i);
                    String body = objResponse.getString("body");
                    JSONObject objBody = new JSONObject(body);
                    String picture = objBody.getString("picture");
                    String source = objBody.getString("source");

                    // download image and save to local file
                    Post post = posts.get(i);
                    File localFile = new File(CommonUtil.getLocalPath(), post.getPostImage());
                    if (localFile.exists()) {
                        continue;
                    }

                    URL fbUrl = new URL(source);
                    InputStream input = fbUrl.openStream();
                    OutputStream output = new FileOutputStream(localFile);
                    byte[] buffer = new byte[1024];
                    int bytesRead = 0;
                    while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                        output.write(buffer, 0, bytesRead);
                    }
                    output.close();
                    input.close();
                }

                // display message that data have been sync
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(
                                SyncBackgroundProcess.this,
                                "Data synchronized",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // return result
        Intent intentResponse = new Intent();
        intentResponse.setAction(ACTION_SYNC);
        intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intentResponse);

        // clear notification
        notifManager.cancel(SYNC_NOTIF_ID);
    }

}
