package id.co.ptdmc.product.android.backgroundprocess;

import id.co.ptdmc.product.android.R;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

/**
 * Background process that handles publishing a comment request.
 *
 * @author Rochmat Santoso
 * */
public final class CommentBackgroundProcess extends IntentService {

    /**
     * Application-specific action for this background process.
     * */
    public static final String ACTION_COMMENT = "id.co.ptdmc.product.android.backgroundprocess.ACTION_COMMENT";

    /**
     * Notification id number.
     * */
    private static final int COMMENT_NOTIF_ID = 23;

    /**
     * Handler to display Toast's text.
     * */
    private Handler handler;

    /**
     * Default constructor.
     * */
    public CommentBackgroundProcess() {
        super("id.co.ptdmc.product.android.backgroundprocess.CommentBackgroundProcess");
        this.handler = new Handler();
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        // create notification
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("Posting comment...")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setTicker("Posting comment...");
        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.notify(COMMENT_NOTIF_ID, notification.build());

        ServiceDaoFactoryUtil util = ServiceDaoFactoryUtil.getInstance();
        String postId = intent.getStringExtra("postId");
        String postSocmedId = intent.getStringExtra("postSocmedId");
        String comment = intent.getStringExtra("comment");
        String[] services = intent.getStringArrayExtra("services");
        for (String service : services) {
            switch (service) {
            case "fb":
                final String result = util.getFacebookService().postComment(postId, postSocmedId, comment);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(
                                CommentBackgroundProcess.this,
                                result,
                                Toast.LENGTH_LONG).show();
                    }
                });
                break;
            case "ig":
                util.getInstagramService().postComment(postId, postSocmedId, comment);
                break;
            default:
                break;
            }
        }

        // return result
        Intent intentResponse = new Intent();
        intentResponse.setAction(ACTION_COMMENT);
        intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intentResponse);

        // clear notification
        notifManager.cancel(COMMENT_NOTIF_ID);
    }

}
