package id.co.ptdmc.product.android.backgroundprocess;

import id.co.ptdmc.product.android.MainActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * {@link PostBackgroundProcess}'s receiver class.
 *
 * @author Rochmat Santoso
 * */
public final class PostProcessReceiver extends BroadcastReceiver {

    /**
     * Activity that calls this receiver.
     * */
    private MainActivity activity;

    /**
     * @return the activity
     */
    public MainActivity getActivity() {
        return activity;
    }

    /**
     * @param paramActivity the activity to set
     */
    public void setActivity(final MainActivity paramActivity) {
        this.activity = paramActivity;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        activity.populateDataPhoto(true);
    }

}
