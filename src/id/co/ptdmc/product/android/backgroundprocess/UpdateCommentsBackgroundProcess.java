package id.co.ptdmc.product.android.backgroundprocess;

import id.co.ptdmc.product.android.MainActivity;
import id.co.ptdmc.product.android.R;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;

import com.facebook.Session;

/**
 * Background process that synchronize local database and media social.
 *
 * @author Rochmat Santoso
 * */
public final class UpdateCommentsBackgroundProcess extends Service {

    /**
     * Notification id.
     * */
    private static final int NOTIF_ID = 66;

    /**
     * WakeLock object that is required to make the device stay on.
     * */
    private WakeLock lock;

    /**
     * Notification manager object.
     * */
    private NotificationManager notifManager;

    /**
     * Notification object.
     * */
    private NotificationCompat.Builder notification;

    @Override
    public void onCreate() {
        super.onCreate();

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        this.notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("New comments from friends")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);
        this.notification.setContentIntent(pendingIntent);

        this.notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(
            final Intent intent, final int flags, final int startId) {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        this.lock = powerManager.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK,
                UpdateCommentsBackgroundProcess.class.getCanonicalName());
        this.lock.acquire();

        ServiceDaoFactoryUtil.getInstance().init(this);
        if (Session.getActiveSession() == null) {
            Session session = Session.openActiveSessionFromCache(this);
            if (session == null) {
                stopSelf();
            }
        }

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            new UpdateTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new UpdateTask().execute();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.lock.release();
    }

    /**
     * Background task that update comments.
     * */
    private class UpdateTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(final Void... params) {
            ServiceDaoFactoryUtil util = ServiceDaoFactoryUtil.getInstance();
            String result = util.getFacebookService().updateComments(null);
            util.getInstagramService().updateComments(null);
            return result;
        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null && result.equals("New Update")) {
                notifManager.notify(NOTIF_ID, notification.build());
            }
            stopSelf();
        }
    }

}
