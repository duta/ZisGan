package id.co.ptdmc.product.android.backgroundprocess;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.JsonReader;
import android.widget.Toast;

/**
 * Background process that handles Instagram request.
 *
 * @author Rochmat Santoso
 * */
public final class InstagramBackgroundProcess extends IntentService {

    /**
     * Application-specific action for this background process.
     * */
    public static final String ACTION_IG = "id.co.ptdmc.product.android.backgroundprocess.ACTION_IG";

    /**
     * Default constructor.
     * */
    public InstagramBackgroundProcess() {
        super("id.co.ptdmc.product.android.backgroundprocess.InstagramBackgroundProcess");
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        InputStream is = null;
        try {
            SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
            String instagramToken = pref.getString("instagramToken", "");

            URL url = new URL("https://api.instagram.com/v1/users/self?access_token=" + instagramToken);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(10000); // in millisecond
            conn.setConnectTimeout(15000); // in millisecond
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();

            is = conn.getInputStream();
            Reader reader = new InputStreamReader(is, "UTF-8");

            String instagramUser = "";
            JsonReader jsonReader = new JsonReader(reader);
            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
              String name = jsonReader.nextName();
              if (name.equals("data")) {
                  jsonReader.beginObject();
                  while (jsonReader.hasNext()) {
                      String fullName = jsonReader.nextName();
                      if (fullName.equals("full_name")) {
                          instagramUser = jsonReader.nextString();
                          break;
                      } else {
                          jsonReader.skipValue();
                      }
                    }
                  break;
              } else {
                  jsonReader.skipValue();
              }
            }
            jsonReader.close();

            // save to shared preferences
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("instagramUser", instagramUser);
            editor.commit();

            Intent intentResponse = new Intent();
            intentResponse.setAction(ACTION_IG);
            intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
            sendBroadcast(intentResponse);
        } catch (MalformedURLException e) {
            Toast.makeText(
                    getApplicationContext(),
                    "Error fetching data, invalid URL",
                    Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(
                    getApplicationContext(),
                    "Error fetching data",
                    Toast.LENGTH_LONG).show();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
