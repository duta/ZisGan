package id.co.ptdmc.product.android.backgroundprocess;

import id.co.ptdmc.product.android.utils.CommonUtil;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

/**
 * {@link UpdateCommentsBackgroundProcess}'s receiver class.
 *
 * @author Rochmat Santoso
 * */
public final class UpdateCommentsProcessReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Intent i = new Intent(context, UpdateCommentsBackgroundProcess.class);
        PendingIntent pendingIntent = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        alarmManager.setInexactRepeating(
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + 10000,
                CommonUtil.SCHEDULER_INTERVAL,
                pendingIntent);
    }

}
