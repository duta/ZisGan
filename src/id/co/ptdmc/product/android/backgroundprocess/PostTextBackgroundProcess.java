package id.co.ptdmc.product.android.backgroundprocess;

import id.co.ptdmc.product.android.R;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

/**
 * Background process that handles posting text request.
 *
 * @author Rochmat Santoso
 * */
public final class PostTextBackgroundProcess extends IntentService {

    /**
     * Application-specific action for this background process.
     * */
    public static final String ACTION_POST_STATUS = "id.co.ptdmc.product.android.backgroundprocess.ACTION_POST_STATUS";

    /**
     * Notification id number.
     * */
    private static final int POST_STATUS_NOTIF_ID = 32;

    /**
     * Handler to display Toast's text.
     * */
    private Handler handler;

    /**
     * Default constructor.
     * */
    public PostTextBackgroundProcess() {
        super("id.co.ptdmc.product.android.backgroundprocess.PostTextBackgroundProcess");
        this.handler = new Handler();
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        // create notification
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("Posting new status...")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setTicker("Posting new status...");
        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.notify(POST_STATUS_NOTIF_ID, notification.build());

        // get extras
        ServiceDaoFactoryUtil util = ServiceDaoFactoryUtil.getInstance();
        String text = intent.getStringExtra("text");
        String[] services = intent.getStringArrayExtra("services");

        for (String service : services) {
            switch (service) {
            case "fb":
                final String result = util.getFacebookService().postText(text);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(
                                PostTextBackgroundProcess.this,
                                result,
                                Toast.LENGTH_LONG).show();
                    }
                });
                break;
            case "ig":
                util.getInstagramService().postText(text);
                break;
            default:
                break;
            }
        }

        // return result
        Intent intentResponse = new Intent();
        intentResponse.setAction(ACTION_POST_STATUS);
        intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intentResponse);

        // clear notification
        notifManager.cancel(POST_STATUS_NOTIF_ID);
    }

}
