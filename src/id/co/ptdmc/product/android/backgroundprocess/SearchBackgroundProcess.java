package id.co.ptdmc.product.android.backgroundprocess;

import id.co.ptdmc.product.android.MainActivity;
import id.co.ptdmc.product.android.daos.PostParseDao;
import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.DbDefinitionUtil;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;

import com.facebook.Session;

/**
 * Background process that handles searching for posts.
 *
 * @author Rochmat Santoso
 * */
public final class SearchBackgroundProcess extends AsyncTask<String, Integer, List<Post>> {

    /**
     * Facebook batch success response code.
     * */
    private static final int RESPONSE_CODE_SUCCESS = 200;

    /**
     * Activity object.
     * */
    private Activity activity;

    /**
     * How many data should we skip.
     * */
    private int skip;

    /**
     * @return the activity
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     * @param paramActivity the activity to set
     */
    public void setActivity(final Activity paramActivity) {
        this.activity = paramActivity;
    }

    /**
     * @return the skip
     */
    public int getSkip() {
        return skip;
    }

    /**
     * @param paramSkip the skip to set
     */
    public void setSkip(final int paramSkip) {
        this.skip = paramSkip;
    }

    @Override
    protected List<Post> doInBackground(final String... params) {
        List<Post> posts = null;
        try {
            JSONArray arrRequest = new JSONArray();
            List<Post> postsToRemove = new ArrayList<>();

            Map<String, Object> filter = new HashMap<>();
            filter.put(DbDefinitionUtil.COLUMN_POST_TEXT, params[0]);
            filter.put("skip", skip);

            PostParseDao dao = ServiceDaoFactoryUtil.getInstance().getPostParseDao();
            posts = dao.selectByFilter(filter);
            for (Post post : posts) {
                if (post.getPostImage() == null) {
                    postsToRemove.add(post);
                    continue;
                }
                // create body to request images to facebook
                String relativeUrl = "/" + post.getPostSocmedId() + "/?fields=source,picture";

                JSONObject obj = new JSONObject();
                obj.put("method", "GET");
                obj.put("relative_url", relativeUrl);

                arrRequest.put(obj);
            }
            posts.removeAll(postsToRemove);

            // create connection
            URL url = new URL("https://graph.facebook.com");

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);

            StringBuilder builder = new StringBuilder();
            builder.append("access_token=");
            builder.append(Session.getActiveSession().getAccessToken());
            builder.append("&batch=");
            builder.append(arrRequest.toString());

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(builder.toString());
            wr.flush();
            wr.close();

            // get response
            int responseCode = conn.getResponseCode();
            if (responseCode == RESPONSE_CODE_SUCCESS) {
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONArray arrResponse = new JSONArray(response.toString());
                for (int i = 0; i < arrResponse.length(); i++) {
                    JSONObject objResponse = arrResponse.getJSONObject(i);
                    String body = objResponse.getString("body");
                    JSONObject objBody = new JSONObject(body);
                    String picture = objBody.optString("picture");
                    String source = objBody.optString("source");

                    Post post = posts.get(i);
                    post.setPostImage(picture);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return posts;
    }

    @Override
    protected void onPostExecute(final List<Post> result) {
        ((MainActivity) activity).populateDataSearch(result, true);
        ((MainActivity) activity).hideSearchProgress();
    }

}
