package id.co.ptdmc.product.android.backgroundprocess;

import id.co.ptdmc.product.android.R;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;

import java.io.File;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

/**
 * Background process that handles posting request.
 *
 * @author Rochmat Santoso
 * */
public final class PostBackgroundProcess extends IntentService {

    /**
     * Application-specific action for this background process.
     * */
    public static final String ACTION_POST = "id.co.ptdmc.product.android.backgroundprocess.ACTION_POST";

    /**
     * Notification id number.
     * */
    private static final int POST_NOTIF_ID = 0;

    /**
     * Handler to display Toast's text.
     * */
    private Handler handler;

    /**
     * Default constructor.
     * */
    public PostBackgroundProcess() {
        super("id.co.ptdmc.product.android.backgroundprocess.PostBackgroundProcess");
        this.handler = new Handler();
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        ServiceDaoFactoryUtil.getInstance().init(this);

        // create notification
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("Posting...")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setTicker("Posting...");
        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.notify(POST_NOTIF_ID, notification.build());

        // get extras
        ServiceDaoFactoryUtil util = ServiceDaoFactoryUtil.getInstance();
        String fileName = intent.getStringExtra("fileName");
        String caption = intent.getStringExtra("caption");
        String[] services = intent.getStringArrayExtra("services");
        File image = new File(fileName);

        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        String pinBB = pref.getString("pinBB", "");
        String waNumber = pref.getString("waNumber", "");
        String smsNumber = pref.getString("smsNumber", "");
        String line = pref.getString("line", "");
        String webAddress = pref.getString("webAddress", "");

        if (pinBB.length() > 0) {
            caption = caption + "\n\rPin BB: " + pinBB;
        }
        if (waNumber.length() > 0) {
            caption = caption + "\n\rWhatsApp: " + waNumber;
        }
        if (smsNumber.length() > 0) {
            caption = caption + "\n\rSMS: " + smsNumber;
        }
        if (line.length() > 0) {
            caption = caption + "\r\nLine: " + line;
        }
        if (webAddress.length() > 0) {
            caption = caption + "\r\nWeb: " + webAddress;
        }

        String result = null;
        for (String service : services) {
            switch (service) {
            case "fb":
                final String postResult = util.getFacebookService().postImage(image, caption);
                result = postResult;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(
                                PostBackgroundProcess.this,
                                postResult,
                                Toast.LENGTH_LONG).show();
                    }
                });
                break;
            case "ig":
                util.getInstagramService().postImage(image, caption);
                break;
            default:
                break;
            }
            // if error posting on fb, do not post on other socmed
            if (result != null && result.contains("Facebook error")) {
                break;
            }
        }

        // return result
        Intent intentResponse = new Intent();
        intentResponse.setAction(ACTION_POST);
        intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(intentResponse);

        // clear notification
        notifManager.cancel(POST_NOTIF_ID);
    }

}
