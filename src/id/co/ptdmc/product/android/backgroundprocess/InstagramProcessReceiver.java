package id.co.ptdmc.product.android.backgroundprocess;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.TextView;

/**
 * {@link InstagramBackgroundProcess}'s receiver class.
 *
 * @author Rochmat Santoso
 * */
public final class InstagramProcessReceiver extends BroadcastReceiver {

    /**
     * TextView to update after receiving the result.
     * */
    private TextView txtInstagramLoginStatus;

    /**
     * @return the txtInstagramLoginStatus
     */
    public TextView getTxtInstagramLoginStatus() {
        return txtInstagramLoginStatus;
    }

    /**
     * @param textView the txtInstagramLoginStatus to set
     */
    public void setTxtInstagramLoginStatus(final TextView textView) {
        this.txtInstagramLoginStatus = textView;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        SharedPreferences pref = context.getSharedPreferences(
                "settings", Context.MODE_PRIVATE);
        String instagramUser = pref.getString("instagramUser", "");
        txtInstagramLoginStatus.setText("You are currently logged in as " + instagramUser);
    }

}
