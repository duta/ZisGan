package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.BitmapCacheUtil;
import id.co.ptdmc.product.android.utils.CommonUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * An adapter class for displaying a list of search result.
 *
 * @author Rochmat Santoso
 * */
public final class MainSearchContentAdapter extends ArrayAdapter<Post> {

    /**
     * Application context.
     * */
    private Context context;

    /**
     * List of thumbnail image.
     * */
    private List<Post> data;

    /**
     * @param ctx application context
     * @param objects the data to display
     * */
    public MainSearchContentAdapter(final Context ctx, final List<Post> objects) {
        super(ctx, R.layout.fragment_main_search_content, objects);

        this.context = ctx;
        this.data = objects;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(
            final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_main_search_content, null);

            ViewHolder holder = new ViewHolder();
            holder.imgViewSearch = (ImageView) convertView.findViewById(R.id.imgViewSearch);
            holder.txtCaptionSearch = (TextView) convertView.findViewById(R.id.txtCaptionSearch);
            holder.txtCreationDateSearch = (TextView) convertView.findViewById(R.id.txtCreationDateSearch);

            convertView.setTag(holder);
        }

        Post obj = data.get(position);
        String date = CommonUtil.formatDateInFriendlyFormat(obj.getPostCreationDate());

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.txtCaptionSearch.setText(obj.getPostText());
        holder.txtCreationDateSearch.setText(date);
        if (holder.task != null) {
            holder.task.cancel(true);
        }
        displayImage(obj, holder);

        return convertView;
    }

    /**
     * Display image in ImageView.
     *
     * @param post the post that contains image
     * @param holder the ViewHolder object
     * */
    private void displayImage(
            final Post post,
            final ViewHolder holder) {
        // reset ImageView
        holder.imgViewSearch.setImageBitmap(null);

        // display post's image
        AsyncTask<Post, Integer, Bitmap> task = new AsyncTask<Post, Integer, Bitmap>() {
            @Override
            protected Bitmap doInBackground(final Post... params) {
                try {
                    BitmapCacheUtil cache = BitmapCacheUtil.getInstance();
                    Bitmap bmp = cache.getBitmapFromCache(post.getPostImage());
                    if (bmp == null) {
                        URL url = new URL(post.getPostImage());
                        InputStream stream = url.openConnection().getInputStream();
                        Bitmap bitmap = BitmapFactory.decodeStream(stream);
                        cache.addBitmapToCache(post.getPostImage(), bitmap);
                        return bitmap;
                    } else {
                        return bmp;
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Bitmap bitmap) {
                holder.imgViewSearch.setImageBitmap(bitmap);
            }
        };
        holder.task = task;

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * A helper class that contains widgets to display search result.
     * */
    static class ViewHolder {
        /**
         * ImageView that holds image result.
         * */
        private ImageView imgViewSearch;

        /**
         * TextView that holds caption.
         * */
        private TextView txtCaptionSearch;

        /**
         * TextView that holds creation date.
         * */
        private TextView txtCreationDateSearch;

        /**
         * The task to load image.
         * */
        private AsyncTask<Post, Integer, Bitmap> task;
    }

}
