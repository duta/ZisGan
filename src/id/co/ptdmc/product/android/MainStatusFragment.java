package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.components.QuickReturnFrameLayout;
import id.co.ptdmc.product.android.daos.PostDao;
import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Fragment class that contains status layout and handles its interactions.
 *
 * @author Rochmat Santoso
 * */
public final class MainStatusFragment extends Fragment implements OnItemClickListener {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        // attach quick return menus
        ListView listStatus = (ListView) getActivity().findViewById(R.id.listStatus);
        listStatus.setOnItemClickListener(this);
        listStatus.setEmptyView(getActivity().findViewById(R.id.emptyStatus));
        ((QuickReturnFrameLayout) getActivity().findViewById(R.id.frameStatus)).attach(listStatus);

        // populate list with data
        populateData(false);
    }

    @Override
    public View onCreateView(
            final LayoutInflater inflater,
            final ViewGroup container,
            final Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_main_status, container, false);
        return rootView;
    }

    /**
     * Populate data to display.
     *
     * @param forceRepopulate force {@link ListView} to be re-populated
     * */
    public void populateData(final boolean forceRepopulate) {
        ListView listStatus = (ListView) getActivity().findViewById(R.id.listStatus);
        ListAdapter adapter = listStatus.getAdapter();
        if (adapter == null || forceRepopulate) {
            Map<String, Object> filter = new HashMap<>();
            filter.put("photoOrText", "text");
            PostDao dao = ServiceDaoFactoryUtil.getInstance().getPostDao();
            List<Post> posts = dao.selectByFilter(filter);
            if (adapter == null) {
                adapter = new MainStatusContentAdapter(getActivity(), posts);
                listStatus.setAdapter(adapter);
            } else {
                ((MainStatusContentAdapter) adapter).clear();
                ((MainStatusContentAdapter) adapter).addAll(posts);
            }
        }
    }

    /**
     * Open new status activity.
     *
     * @param view the view
     * */
    public void postStatus(final View view) {
        Intent intent = new Intent(getActivity(), NewPostStatusActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemClick(
            final AdapterView<?> parent,
            final View view,
            final int position,
            final long id) {
        ListView listData = (ListView) getActivity().findViewById(R.id.listStatus);
        ListAdapter adapter = listData.getAdapter();
        Post post = ((MainStatusContentAdapter) adapter).getItem(position);

        Intent intent = new Intent(getActivity(), DetailStatusActivity.class);
        intent.putExtra("post", post);
        startActivity(intent);
    }

}
