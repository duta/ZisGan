package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.backgroundprocess.PostBackgroundProcess;
import id.co.ptdmc.product.android.backgroundprocess.PostProcessReceiver;
import id.co.ptdmc.product.android.backgroundprocess.PostTextBackgroundProcess;
import id.co.ptdmc.product.android.backgroundprocess.PostTextProcessReceiver;
import id.co.ptdmc.product.android.backgroundprocess.UpdateCommentsBackgroundProcess;
import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.CommonUtil;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;

import java.lang.reflect.Method;
import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionState;

/**
 * Main Activity class.
 *
 * @author Rochmat Santoso
 * */
@SuppressWarnings("deprecation")
public final class MainActivity extends ActionBarActivity implements TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a FragmentPagerAdapter
     * derivative, which will keep every loaded fragment in memory. If this
     * becomes too memory intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private MainFragmentsAdapter fragmentAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager viewPager;

    /**
     * Receiver to refresh data after posting photo.
     * */
    private PostProcessReceiver receiverPostPhoto;

    /**
     * Receiver to refresh data after posting text.
     * */
    private PostTextProcessReceiver receiverPostText;

    /**
     * {@link MainSearchFragment} object.
     * */
    private MainSearchFragment searchFragment;

    /**
     * {@link MainPhotoFragment} object.
     * */
    private MainPhotoFragment photoFragment;

    /**
     * {@link MainStatusFragment} object.
     * */
    private MainStatusFragment statusFragment;

    /**
     * Default constructor.
     * */
    public MainActivity() {
        ServiceDaoFactoryUtil.getInstance().init(this);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        fragmentAdapter = new MainFragmentsAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(fragmentAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(final int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        for (int i = 0; i < fragmentAdapter.getCount(); i++) {
            int icon = -1;
            switch (i) {
                case 0:
                    icon = R.drawable.ic_tab_home;
                    break;
                case 1:
                    icon = R.drawable.ic_tab_photo;
                    break;
                case 2:
                    icon = R.drawable.ic_tab_status;
                    break;
                default:
                    break;
            }
            actionBar.addTab(actionBar.newTab().setIcon(icon).setTabListener(this));
        }

        try {
            final Class<?> clazz = actionBar.getClass();
            final Method embedTabs = clazz.getDeclaredMethod("setHasEmbeddedTabs", boolean.class);
            embedTabs.setAccessible(true);
            embedTabs.invoke(actionBar, true);
        } catch (final Exception e) {
            // Nothing to do
            e.printStackTrace();
        }

        // open facebook session
        if (Session.getActiveSession() == null) {
        	final SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        	String fbUserId = pref.getString("fbUserId", "");
        	final String fbUserName = pref.getString("fbUserName", "");
        	if (fbUserId == null || fbUserId.isEmpty()) {
        		Intent intent = new Intent(this, SettingActivity.class);
                startActivity(intent);
        	} else {
        		Session.openActiveSession(this, true, new Session.StatusCallback() {
                    @Override
                    public void call(
                            final Session session,
                            final SessionState state,
                            final Exception exception) {
                        if (session.isOpened()) {
                        	Toast.makeText(
                                    MainActivity.this,
                                    "Facebook session opened for " + fbUserName,
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
        	}
        }

        startNotification();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        searchFragment = (MainSearchFragment) fragmentAdapter.getItem(0);
        photoFragment = (MainPhotoFragment) fragmentAdapter.getItem(1);
        statusFragment = (MainStatusFragment) fragmentAdapter.getItem(2);

        if (this.receiverPostPhoto == null) {
            // receive update after posting photo
            IntentFilter filter = new IntentFilter(PostBackgroundProcess.ACTION_POST);
            filter.addCategory(Intent.CATEGORY_DEFAULT);

            this.receiverPostPhoto = new PostProcessReceiver();
            receiverPostPhoto.setActivity(this);
            registerReceiver(receiverPostPhoto, filter);
        }

        if (this.receiverPostText == null) {
            // receive update after posting text
            IntentFilter filter = new IntentFilter(PostTextBackgroundProcess.ACTION_POST_STATUS);
            filter.addCategory(Intent.CATEGORY_DEFAULT);

            this.receiverPostText = new PostTextProcessReceiver();
            receiverPostText.setActivity(this);
            registerReceiver(receiverPostText, filter);
        }

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Intent intent = null;
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(
            final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Session session = Session.getActiveSession();
        session.onActivityResult(this, requestCode, resultCode, data);
    }

    /**
     * Populate data to display.
     *
     * @param data list of post as searching result
     * @param forceRepopulate force ListView to be re-populated, otherwise the
     *        list will be added to current list
     * */
    public void populateDataSearch(
            final List<Post> data, final boolean forceRepopulate) {
        searchFragment.populateData(data, forceRepopulate);
    }

    /**
     * Populate data to display.
     *
     * @param forceRepopulate force ListView to be re-populated
     * */
    public void populateDataPhoto(final boolean forceRepopulate) {
        photoFragment.populateData(forceRepopulate);
    }

    /**
     * Populate data to display.
     *
     * @param forceRepopulate force ListView to be re-populated
     * */
    public void populateDataStatus(final boolean forceRepopulate) {
        statusFragment.populateData(forceRepopulate);
    }

    /**
     * View detail activity.
     *
     * @param post the post object required to view the details
     * */
    public void viewDetail(final Post post) {
        photoFragment.viewDetail(post);
    }

    /**
     * Get a picture from camera.
     *
     * @param view the view
     * */
    public void takePicture(final View view) {
        photoFragment.takePicture(view);
    }

    /**
     * Choose an image from media.
     *
     * @param view the view
     * */
    public void chooseImage(final View view) {
        photoFragment.chooseImage(view);
    }

    /**
     * Open status activity.
     *
     * @param view the view
     * */
    public void postStatus(final View view) {
        statusFragment.postStatus(view);
    }

    /**
     * Start service to update comments.
     * */
    private void startNotification() {
        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        boolean isServiceRunning = pref.getBoolean("isServiceRunning", false);
        if (!isServiceRunning) {
            Intent intent = new Intent(this, UpdateCommentsBackgroundProcess.class);
            PendingIntent pendingIntent = PendingIntent.getService(
                    this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pendingIntent);
            alarmManager.setInexactRepeating(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 10000,
                    CommonUtil.SCHEDULER_INTERVAL,
                    pendingIntent);
            startService(intent);

            // save to shared preference
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("isServiceRunning", true);
            editor.commit();
        }
    }

    @Override
    public void onTabReselected(
            final Tab tab, final FragmentTransaction transaction) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabSelected(
            final Tab tab, final FragmentTransaction transaction) {
        switch (tab.getPosition()) {
        case 0:
            tab.setIcon(R.drawable.ic_tab_home_selected);
            break;
        case 1:
            tab.setIcon(R.drawable.ic_tab_photo_selected);
            break;
        case 2:
            tab.setIcon(R.drawable.ic_tab_status_selected);
            break;
        default:
            break;
        }
    }

    @Override
    public void onTabUnselected(
            final Tab tab, final FragmentTransaction transaction) {
        switch (tab.getPosition()) {
        case 0:
            tab.setIcon(R.drawable.ic_tab_home);
            break;
        case 1:
            tab.setIcon(R.drawable.ic_tab_photo);
            break;
        case 2:
            tab.setIcon(R.drawable.ic_tab_status);
            break;
        default:
            break;
        }
    }

    /**
     * Hide progress bar after searching.
     * */
    public void hideSearchProgress() {
        searchFragment.hideSearchProgress();
    }

}
