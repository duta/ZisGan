package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.backgroundprocess.InstagramBackgroundProcess;
import id.co.ptdmc.product.android.backgroundprocess.InstagramProcessReceiver;
import id.co.ptdmc.product.android.backgroundprocess.SyncBackgroundProcess;

import java.util.Arrays;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

/**
 * Setting Activity class.
 *
 * @author Rochmat Santoso
 * */
public final class SettingActivity extends ActionBarActivity {

    /**
     * Facebook login UI helper.
     * */
    private UiLifecycleHelper facebookLoginUIHelper;

    /**
     * Facebook login callback.
     * */
    private Session.StatusCallback facebookLoginCallback = new Session.StatusCallback() {
        @Override
        public void call(
                final Session session,
                final SessionState state,
                final Exception exception) {
            if (state.isOpened()) {
                txtFacebookLoginStatus.setText("Facebook session opened");
            } else if (state.isClosed()) {
                txtFacebookLoginStatus.setText("Facebook session closed");
            }
        }
    };;

    /**
     * Facebook login button.
     * */
    private LoginButton btnFacebookLogin;

    /**
     * Instagram login button.
     * */
    private Button btnInstagramLogin;

    /**
     * TextView that holds facebook login status.
     * */
    private TextView txtFacebookLoginStatus;

    /**
     * TextView that holds instagram login status.
     * */
    private TextView txtInstagramLoginStatus;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_non_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }

        this.facebookLoginUIHelper = new UiLifecycleHelper(this, facebookLoginCallback);
        this.facebookLoginUIHelper.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        this.facebookLoginUIHelper.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        EditText txtPinBB = (EditText) findViewById(R.id.txtPinBB);
        EditText txtWANumber = (EditText) findViewById(R.id.txtWhatsApp);
        EditText txtSMSNumber = (EditText) findViewById(R.id.txtSMS);
        EditText txtLine = (EditText) findViewById(R.id.txtLine);
        EditText txtWebAddress = (EditText) findViewById(R.id.txtWeb);

        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("pinBB", txtPinBB.getText().toString());
        editor.putString("waNumber", txtWANumber.getText().toString());
        editor.putString("smsNumber", txtSMSNumber.getText().toString());
        editor.putString("line", txtLine.getText().toString());
        editor.putString("webAddress", txtWebAddress.getText().toString());
        editor.commit();

        this.facebookLoginUIHelper.onStop();
        super.onStop();
    }

    @Override
    protected void onPause() {
        this.facebookLoginUIHelper.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        this.facebookLoginUIHelper.onResume();
        super.onResume();

        this.txtFacebookLoginStatus = (TextView) findViewById(R.id.txtFacebookLoginStatus);
        this.txtInstagramLoginStatus = (TextView) findViewById(R.id.txtInstagramLoginStatus);

        this.btnFacebookLogin = (LoginButton) findViewById(R.id.btnFacebookLogin);
        this.btnFacebookLogin.setPublishPermissions(Arrays.asList("publish_actions, user_posts"));
        this.btnFacebookLogin.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(final GraphUser user) {
                SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                if (user != null) {
                    editor.putString("fbUserId", user.getId());
                    editor.putString("fbUserName", user.getName());
                    editor.commit();

                    txtFacebookLoginStatus.setText("You are currently logged in as " + user.getName());

                    String isFirstInstall = pref.getString("isFirstInstall", "");
                    if (isFirstInstall == null || isFirstInstall.isEmpty()) {
                        editor.putString("isFirstInstall", "false");
                        editor.commit();

                        Intent senderIntent = new Intent(SettingActivity.this, SyncBackgroundProcess.class);
                        startService(senderIntent);
                    }
                } else {
                    editor.remove("fbUserId");
                    editor.remove("fbUserName");
                    editor.commit();

                    txtFacebookLoginStatus.setText("You are not logged in");
                }
            }
        });

        this.btnInstagramLogin = (Button) findViewById(R.id.btnInstagramLogin);

        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        String instagramToken = pref.getString("instagramToken", "");
        String instagramUser = pref.getString("instagramUser", "");

        if (instagramToken.isEmpty()) {
            this.btnInstagramLogin.setText(R.string.login_instagram);
            this.txtInstagramLoginStatus.setText("You are not logged in");
        } else {
            this.btnInstagramLogin.setText(R.string.logout_instagram);
            if (instagramUser.isEmpty()) {
                this.txtInstagramLoginStatus.setText("Fetching your user data...");

                /* Background process */
                Intent senderIntent = new Intent(this, InstagramBackgroundProcess.class);
                startService(senderIntent);

                IntentFilter filter = new IntentFilter(InstagramBackgroundProcess.ACTION_IG);
                filter.addCategory(Intent.CATEGORY_DEFAULT);

                InstagramProcessReceiver receiver = new InstagramProcessReceiver();
                receiver.setTxtInstagramLoginStatus(txtInstagramLoginStatus);
                registerReceiver(receiver, filter);
            } else {
                this.txtInstagramLoginStatus.setText("You are currently logged in as " + instagramUser);
            }
        }

        // populate EditText
        String pinBB = pref.getString("pinBB", "");
        String waNumber = pref.getString("waNumber", "");
        String smsNumber = pref.getString("smsNumber", "");
        String line = pref.getString("line", "");
        String webAddress = pref.getString("webAddress", "");

        EditText txtPinBB = (EditText) findViewById(R.id.txtPinBB);
        txtPinBB.setText(pinBB);
        EditText txtWANumber = (EditText) findViewById(R.id.txtWhatsApp);
        txtWANumber.setText(waNumber);
        EditText txtSMSNumber = (EditText) findViewById(R.id.txtSMS);
        txtSMSNumber.setText(smsNumber);
        EditText txtLine = (EditText) findViewById(R.id.txtLine);
        txtLine.setText(line);
        EditText txtWebAddress = (EditText) findViewById(R.id.txtWeb);
        txtWebAddress.setText(webAddress);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        this.facebookLoginUIHelper.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Intent intent = null;
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        } else if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
            } else {
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(upIntent);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(
            final int requestCode, final int resultCode, final Intent data) {
        this.facebookLoginUIHelper.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static final class PlaceholderFragment extends Fragment {
        @Override
        public View onCreateView(
                final LayoutInflater inflater,
                final ViewGroup container,
                final Bundle savedInstanceState) {
            View rootView = inflater.inflate(
                    R.layout.fragment_setting, container, false);
            return rootView;
        }
    }

    /**
     * Login to instagram.
     *
     * @param view the view that calls this method
     * */
    public void loginInstagram(final View view) {
        SharedPreferences pref = getSharedPreferences("settings", MODE_PRIVATE);
        String instagramToken = pref.getString("instagramToken", "");

        if (instagramToken.isEmpty()) {
            Intent intent = new Intent(this, LoginInstagramActivity.class);
            startActivity(intent);
        } else {
            // logout instagram
            SharedPreferences.Editor editor = pref.edit();
            editor.remove("instagramToken");
            editor.remove("instagramUser");
            editor.commit();

            // refresh activity
            this.btnInstagramLogin.setText(R.string.login_instagram);
            this.txtInstagramLoginStatus.setText("You are not logged in");
        }
    }

}
