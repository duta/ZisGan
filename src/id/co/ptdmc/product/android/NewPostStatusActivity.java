package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.backgroundprocess.PostTextBackgroundProcess;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Activity class that contains UI to share a new status post.
 *
 * @author Rochmat Santoso
 * */
public final class NewPostStatusActivity extends ActionBarActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Intent intent = null;
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        } else if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
            } else {
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(upIntent);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static final class PlaceholderFragment extends Fragment {
        @Override
        public View onCreateView(
                final LayoutInflater inflater,
                final ViewGroup container,
                final Bundle savedInstanceState) {
            View rootView = inflater.inflate(
                    R.layout.fragment_new_post_status, container, false);
            return rootView;
        }
    }

    /**
     * Share a new post to social medias.
     *
     * @param view the view
     * */
    public void postToSocmed(final View view) {
        EditText txtStatus = (EditText) findViewById(R.id.txtStatus);
        String text = null;
        if (txtStatus.getText() != null) {
            text = txtStatus.getText().toString();
        }

        Intent senderIntent = new Intent(this, PostTextBackgroundProcess.class);
        senderIntent.putExtra("services", new String[] {"fb"});
        senderIntent.putExtra("text", text);
        startService(senderIntent);

        finish();
    }

}
