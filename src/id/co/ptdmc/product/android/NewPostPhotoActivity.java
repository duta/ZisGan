package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.backgroundprocess.PostBackgroundProcess;
import id.co.ptdmc.product.android.utils.CommonUtil;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Activity class that contains UI to share a new image post.
 *
 * @author Rochmat Santoso
 * */
public final class NewPostPhotoActivity extends ActionBarActivity {

    /**
     * Path that holds current image taken by camera or media.
     * */
    private String currentPhotoPath;

    /**
     * Path that holds local image after image processing.
     * */
    private String localPath;

    /**
     * Bitmap object that holds current image taken by camera or media.
     * */
    private Bitmap currentBitmap;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        // TODO: check this, delete temp picture
        /*if (currentPhotoPath != null) {
            File fileTemp = new File(this.currentPhotoPath);
            fileTemp.delete();
        }*/

        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        populateData();

        // calculate screen size
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int screenHeight = displayMetrics.heightPixels;

        // calculate ActionBar height
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(
                    tv.data, getResources().getDisplayMetrics());
        }

        // set image height
        ImageView imgView = (ImageView) findViewById(R.id.imgViewNewPost);
        imgView.getLayoutParams().height = (screenHeight - actionBarHeight) / 2;
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Intent intent = null;
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        } else if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
            } else {
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(upIntent);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static final class PlaceholderFragment extends Fragment {
        @Override
        public View onCreateView(
                final LayoutInflater inflater,
                final ViewGroup container,
                final Bundle savedInstanceState) {
            View rootView = inflater.inflate(
                    R.layout.fragment_new_post_photo, container, false);
            return rootView;
        }
    }

    /**
     * Populate data to display.
     * */
    private void populateData() {
        if (currentBitmap == null) {
            Bundle bundle = getIntent().getExtras();
            this.currentPhotoPath = bundle.getString("filePath");

            // scale and rotate image
            Bitmap bitmap = CommonUtil.scaleAndDecodeBitmap(currentPhotoPath, 612, 816);
            int rotation = CommonUtil.getRotationDegree(this, currentPhotoPath);

            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);

            this.currentBitmap = Bitmap.createBitmap(
                    bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                    matrix, true);

            try {
                // compress and save file
                String timeStamp = CommonUtil.getDateFormatFileName().format(new Date());
                localPath = CommonUtil.getLocalPath() + "ZisGan_" + timeStamp + ".jpg";
                FileOutputStream out = new FileOutputStream(localPath);
                this.currentBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
            } catch (FileNotFoundException e) {
                Toast.makeText(
                        this,
                        "Error compressing image",
                        Toast.LENGTH_LONG).show();
            }

            ImageView imgView = (ImageView) findViewById(R.id.imgViewNewPost);
            imgView.setImageBitmap(currentBitmap);
        }
    }

    /**
     * Share a new post to social medias.
     *
     * @param view the view
     * */
    public void postToSocmed(final View view) {
        EditText txtCaption = (EditText) findViewById(R.id.txtPostCaption);
        String caption = null;
        if (txtCaption.getText() != null) {
            caption = txtCaption.getText().toString();
        }

        Intent senderIntent = new Intent(this, PostBackgroundProcess.class);
        senderIntent.putExtra("services", new String[] {"fb", "ig"});
        senderIntent.putExtra("caption", caption);
        senderIntent.putExtra("fileName", localPath);
        startService(senderIntent);

        finish();
    }

}
