/**
 * A package that contains utility classes.
 *
 * @author Rochmat Santoso
 */
package id.co.ptdmc.product.android.utils;
