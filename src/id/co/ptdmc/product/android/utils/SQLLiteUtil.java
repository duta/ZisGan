package id.co.ptdmc.product.android.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Utility class for SQL Lite database.
 *
 * @author Rochmat Santoso
 * */
public final class SQLLiteUtil extends SQLiteOpenHelper {

    /**
     * Current database version.
     * */
    private static final int DATABASE_VERSION = 1;

    /**
     * Database name.
     * */
    private static final String DATABASE_NAME = "dmc.db";

    /**
     * Create TBL_POST SQL string.
     * */
    private static String SQL_CREATE_TBL_POST;

    /**
     * Create TBL_COMMENT SQL string.
     * */
    private static String SQL_CREATE_TBL_COMMENT;

    /**
     * Delete TBL_POST SQL string.
     * */
    private static String SQL_DELETE_TBL_POST;

    /**
     * Delete TBL_COMMENT SQL string.
     * */
    private static String SQL_DELETE_TBL_COMMENT;

    /**
     * @param context to open or create the database
     * */
    public SQLLiteUtil(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE ");
        sql.append(DbDefinitionUtil.TABLE_POST);
        sql.append(" (");
        sql.append(DbDefinitionUtil._ID);
        sql.append(" INTEGER PRIMARY KEY,");
        sql.append(DbDefinitionUtil.COLUMN_POST_SERVER_ID);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_POST_IMAGE);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_POST_TEXT);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_POST_DATE);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_POST_SOCMED_ID);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_POST_SOCMED_TYPE);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_POST_CURRENT_STATUS);
        sql.append(" TEXT");
        sql.append(" )");

        SQL_CREATE_TBL_POST = sql.toString();

        sql.delete(0, sql.length());
        sql.append("CREATE TABLE ");
        sql.append(DbDefinitionUtil.TABLE_COMMENT);
        sql.append(" (");
        sql.append(DbDefinitionUtil._ID);
        sql.append(" INTEGER PRIMARY KEY,");
        sql.append(DbDefinitionUtil.COLUMN_COMMENT_SERVER_ID);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_COMMENT_TEXT);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_COMMENT_DATE);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_COMMENT_USER_NAME);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_COMMENT_USER_PROFILE);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_COMMENT_SOCMED_ID);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_COMMENT_SOCMED_TYPE);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_POST_ID);
        sql.append(" TEXT,");
        sql.append(DbDefinitionUtil.COLUMN_POST_SERVER_ID);
        sql.append(" TEXT");
        sql.append(" )");

        SQL_CREATE_TBL_COMMENT = sql.toString();

        sql.delete(0, sql.length());
        sql.append("DROP TABLE IF EXISTS ");
        sql.append(DbDefinitionUtil.TABLE_POST);

        SQL_DELETE_TBL_POST = sql.toString();

        sql.delete(0, sql.length());
        sql.append("DROP TABLE IF EXISTS ");
        sql.append(DbDefinitionUtil.TABLE_COMMENT);

        SQL_DELETE_TBL_COMMENT = sql.toString();
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TBL_POST);
        db.execSQL(SQL_CREATE_TBL_COMMENT);
    }

    @Override
    public void onUpgrade(
            final SQLiteDatabase db,
            final int oldVersion,
            final int newVersion) {
        db.execSQL(SQL_DELETE_TBL_POST);
        db.execSQL(SQL_DELETE_TBL_COMMENT);
        onCreate(db);
    }

    @Override
    public void onDowngrade(
            final SQLiteDatabase db,
            final int oldVersion,
            final int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
