package id.co.ptdmc.product.android.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.Environment;
import android.widget.Toast;

/**
 * General purpose utility class.
 *
 * @author Rochmat Santoso
 * */
public final class CommonUtil {

    /**
     * Parse application id.
     * */
    public static final String PARSE_APP_ID = "huYavk2nQY5BByTqpl22ThYFh5xO6qVkAnSYUIG4";

    /**
     * Parse REST key.
     * */
    public static final String PARSE_REST_KEY = "XtMjJK4y4NtrFrgwoY2flM8RfTaZEqR4WzKzFNhA";

    /**
     * Parse request content type.
     * */
    public static final String PARSE_CONTENT_TYPE = "application/json";

    /**
     * Scheduler interval in millisecond.
     * */
    public static final long SCHEDULER_INTERVAL = 10 * 60 * 1000;

    /**
     * Social media type facebook.
     * */
    public static final String SOCMED_TYPE_FACEBOOK = "fb";

    /**
     * Social media type instagram.
     * */
    public static final String SOCMED_TYPE_INSTAGRAM = "ig";

    /**
     * Date formatter instance to parse date from DB.
     * */
    private static final SimpleDateFormat DATE_FORMAT_DB = new SimpleDateFormat(
            "yyyyMMddHHmmss", Locale.getDefault());

    /**
     * Date formatter instance to save date to DB in UTC time zone.
     * */
    private static final SimpleDateFormat DATE_FORMAT_DB_UTC = new SimpleDateFormat(
            "yyyyMMddHHmmss", Locale.getDefault());

    /**
     * Date formatter instance to parse datetime from facebook.
     * */
    private static final SimpleDateFormat DATE_FORMAT_FB = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ss+SSSS", Locale.getDefault());

    /**
     * Date formatter instance that is used as file name formatter.
     * */
    private static final SimpleDateFormat DATE_FORMAT_FILE_NAME = new SimpleDateFormat(
            "yyyyMMdd_HHmmss", Locale.getDefault());

    /**
     * Local path where all files are saved.
     * */
    private static String localFilePath;

    /**
     * Private constructor.
     * */
    private CommonUtil() {
    }

    /**
     * Date formatter instance to parse date from DB.
     *
     * @return date formatter
     * */
    public static SimpleDateFormat getDateFormatDB() {
        return DATE_FORMAT_DB;
    }

    /**
     * Date formatter instance to save date to DB in UTC time zone.
     *
     * @return date formatter
     * */
    public static SimpleDateFormat getDateFormatDBUTC() {
        DATE_FORMAT_DB_UTC.setTimeZone(TimeZone.getTimeZone("UTC"));
        return DATE_FORMAT_DB_UTC;
    }

    /**
     * Date formatter instance to parse datetime from facebook.
     *
     * @return date formatter
     * */
    public static SimpleDateFormat getDateFormatFB() {
        return DATE_FORMAT_FB;
    }

    /**
     * Date formatter instance that is used as file name formatter.
     *
     * @return date formatter
     * */
    public static SimpleDateFormat getDateFormatFileName() {
        return DATE_FORMAT_FILE_NAME;
    }

    /**
     * Get rotation degree of an image.
     *
     * @param context the application context
     * @param filePath file path
     * @return rotation degree
     * */
    public static int getRotationDegree(
            final Context context, final String filePath) {
        int rotate = 0;
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            Toast.makeText(
                    context,
                    "Error processing image rotation",
                    Toast.LENGTH_LONG).show();
        }
        return rotate;
    }

    /**
     * Create a new bitmap from a file and scale it down to be around the
     * desired width and height.
     *
     * @param filePath original file path name
     * @param desiredWidth desired width after scaling
     * @param desiredHeight desired height after scaling
     * @return the bitmap
     * */
    public static Bitmap scaleAndDecodeBitmap(
            final String filePath,
            final float desiredWidth,
            final float desiredHeight) {
        // get image's real height and width
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(filePath, options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;

        // calculate scale size
        int scaleSize = 1;
        if (imageHeight > desiredHeight || imageWidth > desiredWidth) {
            final int halfHeight = imageHeight / 2;
            final int halfWidth = imageWidth / 2;

            // Calculate the largest scaleSize value that is a power of 2 and
            // keeps height and width larger than the desired height and width.
            while ((halfHeight / scaleSize) > desiredHeight
                    && (halfWidth / scaleSize) > desiredWidth) {
                scaleSize *= 2;
            }
        }

        //decode image
        options.inSampleSize = scaleSize;
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    /**
     * Format comment's date in friendly format.
     *
     * @param date the date to format
     * @return formatted date
     * */
    public static String formatDateInFriendlyFormat(final Date date) {
        TimeZone zone = TimeZone.getDefault();
        boolean isInDaylightSavingTime = zone.inDaylightTime(new Date());

        int utc = zone.getRawOffset();
        int dst = isInDaylightSavingTime ? zone.getDSTSavings() : 0;

        long then = new Date(date.getTime() + utc + dst).getTime();
        long now = new Date().getTime();

        long seconds = (now - then) / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        String result = null;
        long num = 0;
        if (days > 0) {
            num = days;
            result = days + " day";
        } else if (hours > 0) {
            num = hours;
            result = hours + " hour";
        } else if (minutes > 0) {
            num = minutes;
            result = minutes + " minute";
        } else {
            num = seconds;
            result = Math.abs(seconds) + " second";
        }
        if (num > 1) {
            result += "s";
        }
        return result + " ago";
    }

    /**
     * Get local path where all files are saved.
     *
     * @return the path
     * */
    public static String getLocalPath() {
        if (localFilePath == null) {
            File dir = new File(Environment.getExternalStorageDirectory(), "ZisGan");
            dir.mkdir();

            localFilePath = dir.getAbsolutePath() + "/";

            File nomedia = new File(dir, ".nomedia");
            try {
                nomedia.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return localFilePath;
    }

}
