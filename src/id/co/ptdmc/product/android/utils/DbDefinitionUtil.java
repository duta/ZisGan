package id.co.ptdmc.product.android.utils;

import android.provider.BaseColumns;

/**
 * Database Definition utility class.
 *
 * @author Rochmat Santoso
 * */
public final class DbDefinitionUtil implements BaseColumns {

    /**
     * Private constructor.
     * */
    private DbDefinitionUtil() {
    }

    /**
     * Table TBL_POST.
     * */
    public static final String TABLE_POST = "tbl_post";

    /**
     * Table TBL_COMMENT.
     * */
    public static final String TABLE_COMMENT = "tbl_comment";

    /**
     * POST_SERVER_ID of TBL_POST.
     * */
    public static final String COLUMN_POST_SERVER_ID = "post_server_id";

    /**
     * POST_IMAGE of TBL_POST.
     * */
    public static final String COLUMN_POST_IMAGE = "post_image";

    /**
     * POST_TEXT of TBL_POST.
     * */
    public static final String COLUMN_POST_TEXT = "post_text";

    /**
     * POST_DATE of TBL_POST.
     * */
    public static final String COLUMN_POST_DATE = "post_date";

    /**
     * POST_SOCMED_ID of TBL_POST.
     * */
    public static final String COLUMN_POST_SOCMED_ID = "post_socmed_id";

    /**
     * POST_SOCMED_TYPE of TBL_POST.
     * */
    public static final String COLUMN_POST_SOCMED_TYPE = "post_socmed_type";

    /**
     * POST_CURRENT_STATUS of TBL_POST.
     * */
    public static final String COLUMN_POST_CURRENT_STATUS = "post_current_status";

    /**
     * COMMENT_SERVER_ID of TBL_COMMENT.
     * */
    public static final String COLUMN_COMMENT_SERVER_ID = "comment_server_id";

    /**
     * COMMENT_TEXT of TBL_COMMENT.
     * */
    public static final String COLUMN_COMMENT_TEXT = "comment_text";

    /**
     * COMMENT_DATE of TBL_COMMENT.
     * */
    public static final String COLUMN_COMMENT_DATE = "comment_date";

    /**
     * COMMENT_USER_NAME of TBL_COMMENT.
     * */
    public static final String COLUMN_COMMENT_USER_NAME = "comment_user_name";

    /**
     * COMMENT_USER_PROFILE of TBL_COMMENT.
     * */
    public static final String COLUMN_COMMENT_USER_PROFILE = "comment_user_profile";

    /**
     * COMMENT_SOCMED_ID of TBL_COMMENT.
     * */
    public static final String COLUMN_COMMENT_SOCMED_ID = "comment_socmed_id";

    /**
     * COMMENT_SOCMED_TYPE of TBL_COMMENT.
     * */
    public static final String COLUMN_COMMENT_SOCMED_TYPE = "comment_socmed_type";

    /**
     * POST_ID of TBL_COMMENT.
     * */
    public static final String COLUMN_POST_ID = "post_id";

}
