package id.co.ptdmc.product.android.utils;

import id.co.ptdmc.product.android.daos.CommentDao;
import id.co.ptdmc.product.android.daos.CommentParseDao;
import id.co.ptdmc.product.android.daos.PostDao;
import id.co.ptdmc.product.android.daos.PostParseDao;
import id.co.ptdmc.product.android.services.FacebookService;
import id.co.ptdmc.product.android.services.InstagramService;
import android.content.Context;

/**
 * A factory class that is responsible to instantiate service and DAO classes.
 *
 * @author Rochmat Santoso
 * */
public final class ServiceDaoFactoryUtil {

    /**
     * Singleton instance.
     * */
    private static final ServiceDaoFactoryUtil INSTANCE = new ServiceDaoFactoryUtil();

    /**
     * SQL Lite utility object.
     * */
    private SQLLiteUtil sqlLiteUtil;

    /**
     * Activity that calls this service.
     * */
    private Context activity;

    /**
     * PostDao object.
     * */
    private PostDao postDao;

    /**
     * CommentDao object.
     * */
    private CommentDao commentDao;

    /**
     * FacebookService object.
     * */
    private FacebookService facebookService;

    /**
     * InstagramService object.
     * */
    private InstagramService instagramService;

    /**
     * PostParseDao object.
     * */
    private PostParseDao postParseDao;

    /**
     * CommentParseDao object.
     * */
    private CommentParseDao commentParseDao;

    /**
     * Private constructor.
     * */
    private ServiceDaoFactoryUtil() {
    }

    /**
     * Get instance of this class.
     *
     * @return instance of this class
     * */
    public static ServiceDaoFactoryUtil getInstance() {
        return INSTANCE;
    }

    /**
     * Initialize this class. This method must be called before calling other
     * methods.
     *
     * @param paramActivity the activity that calls this service
     * */
    public void init(final Context paramActivity) {
        this.sqlLiteUtil = new SQLLiteUtil(paramActivity);
        this.activity = paramActivity;

        if (postDao != null) {
            postDao.setApplicationContext(activity);
        }
        if (commentDao != null) {
            commentDao.setApplicationContext(activity);
        }
        if (facebookService != null) {
            facebookService.setActivity(activity);
        }
        if (instagramService != null) {
            instagramService.setActivity(activity);
        }
        if (postParseDao != null) {
            postParseDao.setApplicationContext(activity);
        }
        if (commentParseDao != null) {
            commentParseDao.setApplicationContext(activity);
        }
    }

    /**
     * @return the postDao
     * */
    public PostDao getPostDao() {
        if (postDao == null) {
            postDao = new PostDao(sqlLiteUtil, activity);
        }
        return postDao;
    }

    /**
     * @return the commentDao
     * */
    public CommentDao getCommentDao() {
        if (commentDao == null) {
            commentDao = new CommentDao(sqlLiteUtil, activity);
        }
        return commentDao;
    }

    /**
     * @return the facebookService
     * */
    public FacebookService getFacebookService() {
        if (facebookService == null) {
            facebookService = new FacebookService(activity);
            facebookService.setPostDao(getPostDao());
            facebookService.setCommentDao(getCommentDao());
            facebookService.setPostParseDao(getPostParseDao());
            facebookService.setCommentParseDao(getCommentParseDao());
        }
        return facebookService;
    }

    /**
     * @return the instagramService
     * */
    public InstagramService getInstagramService() {
        if (instagramService == null) {
            instagramService = new InstagramService(activity);
            instagramService.setPostDao(getPostDao());
            instagramService.setCommentDao(getCommentDao());
            instagramService.setPostParseDao(getPostParseDao());
            instagramService.setCommentParseDao(getCommentParseDao());
        }
        return instagramService;
    }

    /**
     * @return the postParseDao
     */
    public PostParseDao getPostParseDao() {
        if (postParseDao == null) {
            postParseDao = new PostParseDao(sqlLiteUtil, activity);
        }
        return postParseDao;
    }

    /**
     * @return the commentParseDao
     */
    public CommentParseDao getCommentParseDao() {
        if (commentParseDao == null) {
            commentParseDao = new CommentParseDao(sqlLiteUtil, activity);
        }
        return commentParseDao;
    }

}
