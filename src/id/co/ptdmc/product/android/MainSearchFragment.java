package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.backgroundprocess.SearchBackgroundProcess;
import id.co.ptdmc.product.android.components.QuickReturnFrameLayout;
import id.co.ptdmc.product.android.models.Post;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

/**
 * Fragment class that contains search layout and handles its interactions.
 *
 * @author Rochmat Santoso
 * */
public final class MainSearchFragment extends Fragment implements
        OnQueryTextListener, OnItemClickListener, SwipyRefreshLayout.OnRefreshListener {

    /**
     * SearchView object.
     * */
    private SearchView searchView;

    /**
     * ProgressBar object.
     * */
    private ProgressBar progressSearch;

    /**
     * A list that contains search result.
     * */
    private List<Post> searchResultList;

    /**
     * SwipyRefreshLayout object.
     * */
    private SwipyRefreshLayout swipeRefreshSearch;

    /**
     * Current query string.
     * */
    private String queryString;

    /**
     * A flag indicating that we are in the middle of fetching more data.
     * */
    private boolean isFetchingNextData;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        progressSearch = (ProgressBar) getActivity().findViewById(R.id.progressSearch);

        searchView = (SearchView) getActivity().findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);

        for (TextView textView : getChildren()) {
            textView.setTextColor(Color.WHITE);
            textView.setHintTextColor(Color.WHITE);
        }

        swipeRefreshSearch = (SwipyRefreshLayout) getActivity().findViewById(R.id.swipeRefreshSearch);
        swipeRefreshSearch.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        swipeRefreshSearch.setOnRefreshListener(this);
        swipeRefreshSearch.setColorSchemeResources(
                R.color.red, R.color.orange, R.color.green, R.color.blue);

        // attach quick return menus
        ListView listSearch = (ListView) getActivity().findViewById(R.id.listSearch);
        listSearch.setOnItemClickListener(this);
        listSearch.setEmptyView(getActivity().findViewById(R.id.emptySearch));
        ((QuickReturnFrameLayout) getActivity().findViewById(R.id.frameSearch)).attach(listSearch);

        if (searchResultList != null) {
            populateData(searchResultList, false);
        }
    }

    @Override
    public View onCreateView(
            final LayoutInflater inflater,
            final ViewGroup container,
            final Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_main_search, container, false);
        return rootView;
    }

    @Override
    public void onRefresh(final SwipyRefreshLayoutDirection direction) {
        isFetchingNextData = true;

        SearchBackgroundProcess task = new SearchBackgroundProcess();
        task.setActivity(getActivity());
        task.setSkip(searchResultList.size());

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, queryString);
        } else {
            task.execute(queryString);
        }
    }

    @Override
    public boolean onQueryTextSubmit(final String query) {
        if (query != null && query.length() > 0) {
            queryString = query;
            isFetchingNextData = false;

            SearchBackgroundProcess task = new SearchBackgroundProcess();
            task.setActivity(getActivity());
            task.setSkip(0);

            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, query);
            } else {
                task.execute(query);
            }

            progressSearch.setVisibility(View.VISIBLE);
            return true;
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        // not implemented
        return false;
    }

    @Override
    public void onItemClick(
            final AdapterView<?> parent,
            final View view,
            final int position,
            final long id) {
        ListView listData = (ListView) getActivity().findViewById(R.id.listSearch);
        ListAdapter adapter = listData.getAdapter();
        Post post = ((MainSearchContentAdapter) adapter).getItem(position);

        Intent intent = new Intent(getActivity(), DetailSearchActivity.class);
        intent.putExtra("post", post);
        startActivity(intent);
    }

    /**
     * Populate data to display.
     *
     * @param data list of post as searching result
     * @param forceRepopulate force ListView to be re-populated
     * */
    public void populateData(
            final List<Post> data, final boolean forceRepopulate) {
        ListView listData = (ListView) getActivity().findViewById(R.id.listSearch);
        if (listData == null) {
            return;
        }
        ListAdapter adapter = listData.getAdapter();

        if (isFetchingNextData) {
            ((MainSearchContentAdapter) adapter).addAll(data);
            return;
        }

        searchResultList = data;
        if (adapter == null || forceRepopulate) {
            if (adapter == null) {
                adapter = new MainSearchContentAdapter(getActivity(), data);
                ((MainSearchContentAdapter) adapter).setNotifyOnChange(true);
                listData.setAdapter(adapter);
            } else {
                ((MainSearchContentAdapter) adapter).clear();
                ((MainSearchContentAdapter) adapter).addAll(data);
                ((MainSearchContentAdapter) adapter).notifyDataSetChanged();
            }
        }
    }

    /**
     * Get children of searchView.
     *
     * @return a list of TextView that are children of searchView
     * */
    private List<TextView> getChildren() {
        return getChildren(searchView, new ArrayList<TextView>());
    }

    /**
     * Recursive method to get children of ViewGroup.
     *
     * @param viewGroup the ViewGroup that contains TextView
     * @param childrenFound current children
     * @return a list of TextView that are children of searchView
     * */
    private List<TextView> getChildren(
            final ViewGroup viewGroup,
            final List<TextView> childrenFound) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            final View child = viewGroup.getChildAt(i);
            if (TextView.class.isAssignableFrom(child.getClass())) {
                childrenFound.add((TextView) child);
            }
            if (child instanceof ViewGroup) {
                getChildren((ViewGroup) child, childrenFound);
            }
        }

        return childrenFound;
    }

    /**
     * Hide progress bar.
     * */
    public void hideSearchProgress() {
        progressSearch.setVisibility(View.GONE);
        swipeRefreshSearch.setRefreshing(false);
        isFetchingNextData = false;
    }

}
