package id.co.ptdmc.product.android.models;

import java.util.Date;

/**
 * TBL_COMMENT data model.
 *
 * @author Rochmat Santoso
 * */
public final class Comment extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = -5854332326958614584L;

    /**
     * Primary key of TBL_COMMENT.
     * */
    private String commentId;

    /**
     * COMMENT_SERVER_ID column representation.
     * */
    private String commentServerId;

    /**
     * COMMENT_TEXT column representation.
     * */
    private String commentText;

    /**
     * COMMENT_DATE column representation.
     * */
    private Date commentDate;

    /**
     * COMMENT_USER_NAME column representation.
     * */
    private String commentUserName;

    /**
     * COMMENT_USER_PROFILE column representation.
     * */
    private String commentUserProfile;

    /**
     * COMMENT_SOCMED_ID column representation.
     * */
    private String commentSocmedId;

    /**
     * COMMENT_SOCMED_TYPE column representation.
     * */
    private String commentSocmedType;

    /**
     * POST_ID column representation.
     * */
    private String postId;

    /**
     * POST_SERVER_ID column representation.
     * */
    private String postServerId;

    /**
     * @return the commentId
     */
    public String getCommentId() {
        return commentId;
    }

    /**
     * @param paramCommentId the commentId to set
     */
    public void setCommentId(final String paramCommentId) {
        this.commentId = paramCommentId;
    }

    /**
     * @return the commentServerId
     */
    public String getCommentServerId() {
        return commentServerId;
    }

    /**
     * @param paramCommentServerId the commentServerId to set
     */
    public void setCommentServerId(final String paramCommentServerId) {
        this.commentServerId = paramCommentServerId;
    }

    /**
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * @param paramCommentText the commentText to set
     */
    public void setCommentText(final String paramCommentText) {
        this.commentText = paramCommentText;
    }

    /**
     * @return the commentDate
     */
    public Date getCommentDate() {
        return commentDate;
    }

    /**
     * @param paramCommentDate the commentDate to set
     */
    public void setCommentDate(final Date paramCommentDate) {
        this.commentDate = paramCommentDate;
    }

    /**
     * @return the commentUserName
     */
    public String getCommentUserName() {
        return commentUserName;
    }

    /**
     * @param paramCommentUserName the commentUserName to set
     */
    public void setCommentUserName(final String paramCommentUserName) {
        this.commentUserName = paramCommentUserName;
    }

    /**
     * @return the commentUserProfile
     */
    public String getCommentUserProfile() {
        return commentUserProfile;
    }

    /**
     * @param paramCommentUserProfile the commentUserProfile to set
     */
    public void setCommentUserProfile(final String paramCommentUserProfile) {
        this.commentUserProfile = paramCommentUserProfile;
    }

    /**
     * @return the commentSocmedId
     */
    public String getCommentSocmedId() {
        return commentSocmedId;
    }

    /**
     * @param paramCommentSocmedId the commentSocmedId to set
     */
    public void setCommentSocmedId(final String paramCommentSocmedId) {
        this.commentSocmedId = paramCommentSocmedId;
    }

    /**
     * @return the commentSocmedType
     */
    public String getCommentSocmedType() {
        return commentSocmedType;
    }

    /**
     * @param paramCommentSocmedType the commentSocmedType to set
     */
    public void setCommentSocmedType(final String paramCommentSocmedType) {
        this.commentSocmedType = paramCommentSocmedType;
    }

    /**
     * @return the postId
     */
    public String getPostId() {
        return postId;
    }

    /**
     * @param paramPostId the postId to set
     */
    public void setPostId(final String paramPostId) {
        this.postId = paramPostId;
    }

    /**
     * @return the postServerId
     */
    public String getPostServerId() {
        return postServerId;
    }

    /**
     * @param paramPostServerId the postServerId to set
     */
    public void setPostServerId(final String paramPostServerId) {
        this.postServerId = paramPostServerId;
    }

    @Override
    public String toString() {
        return "Comment [commentId=" + commentId + ", commentServerId="
                + commentServerId + ", commentText=" + commentText
                + ", commentDate=" + commentDate + ", commentUserName="
                + commentUserName + ", commentUserProfile="
                + commentUserProfile + ", commentSocmedId=" + commentSocmedId
                + ", commentSocmedType=" + commentSocmedType + ", postId="
                + postId + ", postServerId=" + postServerId + "]";
    }

}
