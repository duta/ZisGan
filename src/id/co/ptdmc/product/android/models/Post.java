package id.co.ptdmc.product.android.models;

import java.util.Date;
import java.util.List;

/**
 * TBL_POST data model.
 *
 * @author Rochmat Santoso
 * */
public final class Post extends BaseBean {

    /**
     *
     */
    private static final long serialVersionUID = 8183278079401509160L;

    /**
     * Primary key of TBL_POST.
     * */
    private String postId;

    /**
     * POST_SERVER_ID column representation.
     * */
    private String postServerId;

    /**
     * POST_IMAGE column representation.
     * */
    private String postImage;

    /**
     * POST_TEXT column representation.
     * */
    private String postText;

    /**
     * POST_DATE column representation.
     * */
    private Date postCreationDate;

    /**
     * POST_SOCMED_ID column representation. This id is defaulted to Facebook
     * id. If there is a need to connect to another social media, one should
     * create another postSocmedId.
     * */
    private String postSocmedId;

    /**
     * POST_SOCMED_TYPE column representation.
     * */
    private String postSocmedType;

    /**
     * POST_CURRENT_STATUS column representation.
     * */
    private String postCurrStatus;

    /**
     * Comments available on this post.
     * */
    private List<Comment> comments;

    /**
     * @return the postId
     */
    public String getPostId() {
        return postId;
    }

    /**
     * @param paramPostId the postId to set
     */
    public void setPostId(final String paramPostId) {
        this.postId = paramPostId;
    }

    /**
     * @return the postServerId
     */
    public String getPostServerId() {
        return postServerId;
    }

    /**
     * @param paramPostServerId the postServerId to set
     */
    public void setPostServerId(final String paramPostServerId) {
        this.postServerId = paramPostServerId;
    }

    /**
     * @return the postImage
     */
    public String getPostImage() {
        return postImage;
    }

    /**
     * @param paramPostImage the postImage to set
     */
    public void setPostImage(final String paramPostImage) {
        this.postImage = paramPostImage;
    }

    /**
     * @return the postText
     */
    public String getPostText() {
        return postText;
    }

    /**
     * @param paramPostText the postText to set
     */
    public void setPostText(final String paramPostText) {
        this.postText = paramPostText;
    }

    /**
     * @return the postCreationDate
     */
    public Date getPostCreationDate() {
        return postCreationDate;
    }

    /**
     * @param paramPostCreationDate the postCreationDate to set
     */
    public void setPostCreationDate(final Date paramPostCreationDate) {
        this.postCreationDate = paramPostCreationDate;
    }

    /**
     * @return the postSocmedId
     */
    public String getPostSocmedId() {
        return postSocmedId;
    }

    /**
     * @param paramPostSocmedId the postSocmedId to set
     */
    public void setPostSocmedId(final String paramPostSocmedId) {
        this.postSocmedId = paramPostSocmedId;
    }

    /**
     * @return the postSocmedType
     */
    public String getPostSocmedType() {
        return postSocmedType;
    }

    /**
     * @param paramPostSocmedType the postSocmedType to set
     */
    public void setPostSocmedType(final String paramPostSocmedType) {
        this.postSocmedType = paramPostSocmedType;
    }

    /**
     * @return the postCurrStatus
     */
    public String getPostCurrStatus() {
        return postCurrStatus;
    }

    /**
     * @param paramPostCurrStatus the postCurrStatus to set
     */
    public void setPostCurrStatus(final String paramPostCurrStatus) {
        this.postCurrStatus = paramPostCurrStatus;
    }

    /**
     * @return the comments
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * @param paramComments the comments to set
     */
    public void setComments(final List<Comment> paramComments) {
        this.comments = paramComments;
    }

    @Override
    public String toString() {
        return "Post [postId=" + postId + ", postServerId=" + postServerId
                + ", postImage=" + postImage + ", postText=" + postText
                + ", postCreationDate=" + postCreationDate + ", postSocmedId="
                + postSocmedId + ", postSocmedType=" + postSocmedType
                + ", postCurrStatus=" + postCurrStatus + "]";
    }

}
