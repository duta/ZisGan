/**
 * A package that contains model classes.
 *
 * @author Rochmat Santoso
 */
package id.co.ptdmc.product.android.models;
