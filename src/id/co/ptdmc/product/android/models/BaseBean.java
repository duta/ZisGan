package id.co.ptdmc.product.android.models;

import java.io.Serializable;

/**
 * Base class for all bean data model classes.
 *
 * @author Rochmat Santoso
 * */
public class BaseBean implements IBean, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6250252297440682081L;

}
