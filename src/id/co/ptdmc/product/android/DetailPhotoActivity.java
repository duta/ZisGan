package id.co.ptdmc.product.android;

import id.co.ptdmc.product.android.backgroundprocess.CommentBackgroundProcess;
import id.co.ptdmc.product.android.components.QuickReturnFrameLayout;
import id.co.ptdmc.product.android.daos.CommentDao;
import id.co.ptdmc.product.android.daos.PostDao;
import id.co.ptdmc.product.android.models.Comment;
import id.co.ptdmc.product.android.models.Post;
import id.co.ptdmc.product.android.utils.CommonUtil;
import id.co.ptdmc.product.android.utils.DbDefinitionUtil;
import id.co.ptdmc.product.android.utils.ServiceDaoFactoryUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Detail photo activity class.
 *
 * @author Rochmat Santoso
 * */
public final class DetailPhotoActivity extends ActionBarActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // calculate screen size
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int screenHeight = displayMetrics.heightPixels;

        // calculate ActionBar height
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(
                    tv.data, getResources().getDisplayMetrics());
        }

        // set image height
        ImageView imgView = (ImageView) findViewById(R.id.imgViewDetail);
        imgView.getLayoutParams().height = (screenHeight - actionBarHeight) / 2;

        // attach quick return bar
        ListView listComment = (ListView) findViewById(R.id.listComment);
        listComment.setEmptyView(findViewById(R.id.emptyCommentPhoto));
        ((QuickReturnFrameLayout) findViewById(R.id.frame)).attach(listComment);

        populateData();
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Intent intent = null;
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        } else if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
            } else {
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(upIntent);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static final class PlaceholderFragment extends Fragment {
        @Override
        public View onCreateView(
                final LayoutInflater inflater,
                final ViewGroup container,
                final Bundle savedInstanceState) {
            View rootView = inflater.inflate(
                    R.layout.fragment_detail_photo, container, false);
            return rootView;
        }
    }

    /**
     * Populate data to display.
     * */
    private void populateData() {
        Bundle bundle = getIntent().getExtras();
        Post post = (Post) bundle.get("post");

        String currStatus = post.getPostCurrStatus();
        if (currStatus != null) {
            post.setPostCurrStatus(null);
            PostDao dao = ServiceDaoFactoryUtil.getInstance().getPostDao();
            dao.update(post);
        }

        String path = CommonUtil.getLocalPath() + post.getPostImage();
        Bitmap bitmap = BitmapFactory.decodeFile(path);

        ImageView imgView = (ImageView) findViewById(R.id.imgViewDetail);
        imgView.setImageBitmap(bitmap);

        Map<String, Object> filter = new HashMap<>();
        filter.put(DbDefinitionUtil.COLUMN_POST_ID, post.getPostId());

        CommentDao dao = ServiceDaoFactoryUtil.getInstance().getCommentDao();
        List<Comment> comments = dao.selectByFilter(filter);
        ListView listComment = (ListView) findViewById(R.id.listComment);
        listComment.setAdapter(new DetailContentAdapter(this, comments));

        TextView txtCaption = (TextView) findViewById(R.id.txtCaption);
        txtCaption.setText(post.getPostText());
    }

    /**
     * Post a new comment.
     *
     * @param view the view
     * */
    public void postComment(final View view) {
        Bundle bundle = getIntent().getExtras();
        Post post = (Post) bundle.get("post");

        EditText txtComment = (EditText) findViewById(R.id.txtPostComment);
        String comment = null;
        if (txtComment.getText() != null) {
            comment = txtComment.getText().toString();
        } else {
            return;
        }

        Intent senderIntent = new Intent(this, CommentBackgroundProcess.class);
        senderIntent.putExtra("services", new String[] {"fb", "ig"});
        senderIntent.putExtra("postId", post.getPostId());
        senderIntent.putExtra("postSocmedId", post.getPostSocmedId());
        senderIntent.putExtra("comment", comment);
        startService(senderIntent);

        finish();
    }

}
